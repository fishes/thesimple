/**
 * @FileName ITxCallbak.java
 * @Package tk.lx.dao
 * @author muxue
 * @date Nov 13, 2015
 */
package tk.lx.dao;



/**
 * @ClassName: ITxCallbak
 * @Description 事务操作中的原子方法接口
 * @author muxue
 * @date Nov 13, 2015
 */
public interface ITxCallback
{
	/*
	 * @return true : 可以提交，否则回滚
	 */
	boolean run();
}
