package tk.lx.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import tk.lx.dao.helper.RecordBeanConverter;
import tk.lx.dao.mapping.DefaultMapping;
import tk.lx.dao.mapping.IResultMapping;
import tk.lx.util.DbpoolUtil;

/**
 * ClassName: BaseDao
 * Description: dao的基类，封装常用的CRUD原子性操作
 * Date: Oct 31, 2015
 *
 * @author muxue
 */

public abstract class BaseDao
{
	/**
	 * 默认使用“默认的映射器”
	 * 做成线程局部变量，防止继承类做成单例，并发时出错
	 */
	private ThreadLocal<IResultMapping> resultMapping = new ThreadLocal<>();
	private ThreadLocal<Connection> localConnection = new ThreadLocal<>();
	private ThreadLocal<Boolean> ifAllTxSuccess = new ThreadLocal<>();

	
	/**
	 * 获取一个BaseDao的默认实现实例，用于一些简单的数据库操作
	 */
	private static BaseDao baseDao = new BaseDao(){};
	public static BaseDao getInstance()
	{
		return baseDao;
	}
	
	/**
	 * constructor
	 */
	public BaseDao()
	{
		configResultMapping();
	}
	
	/**
	 * 暂时用于子类配置ResultMapping
	 */
	protected void configResultMapping()
	{
		//默认使用DefaultMapping规则
		resultMapping.set(new DefaultMapping());
	}
	
	/**
	 * 结果以HashMap的集合形式返回所有记录
	 */
	public ArrayList<HashMap<String, Object>> selectList(String sql, Object... args)
	{
		ArrayList<HashMap<String, Object>> records = new ArrayList<>();
		Connection connection = localConnection.get();
		boolean isTxExist = true;
		if (connection == null)
		{
			connection = DbpoolUtil.getConnection();
			if (connection == null)
				return null;
			
			localConnection.set(connection);
			isTxExist = false;
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;

		try
		{
			ps = connection.prepareStatement(sql);

			// 设置sql里的参数
			if (args != null)
				for (int i = 1; i <= args.length; i++)
					ps.setObject(i, args[i - 1]);

			rs = ps.executeQuery();
			rsmd = rs.getMetaData();

			while (rs.next())
			{
				HashMap<String, Object> record = new HashMap<>();

				for (int i = 1; i <= rsmd.getColumnCount(); i++)
				{
					//FIXME:暂时不处理XLOB
					Object instance = rs.getObject(i);
					record.put(rsmd.getColumnName(i), instance);
				}

				records.add(record);
			}
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
			records = null;
		}
		finally
		{
			if (!isTxExist)
			{
				close(connection, ps);
				localConnection.remove();
			}
		}

		return records;
	}


	/**
	 * 取查询结果（结果只有一条）
	 */
	public HashMap<String, Object> selectOne(String sql, Object... args)
	{
		ArrayList<HashMap<String, Object>> records = selectList(sql, args);
		if (records != null && records.size() != 0)
			return records.get(0);
		
		return null;
	}

	/**
	 * 将一条记录转换成bean
	 * 最后一个参数就作为数据库返回结果和bean的属性映射器
	 * 如果最后一个参数不是IResultMapping的话，那么就使用默认的
	 * 也就是***不转换***
	 */
	public <T> T selectBean(Class<T> clazz, String sql, Object ... args)
	{
		if (resultMapping.get() == null)
			// Bug Fix: 当使用单例dao时，不同线程来访问，绑定的ResultMapping不会被正确设置
			// 			因为配置ResultMapping的代码只在构造时被调用并设置进那时的thread local变量里
			//			而对于之后的thread是不起作用的
			//resultMapping.set(new DefaultMapping());
			configResultMapping();
		
		IResultMapping mapping = null;
		if (args.length != 0 && args[args.length-1] instanceof IResultMapping)
		{
			mapping = (IResultMapping) args[args.length-1];
			args = Arrays.copyOf(args, args.length-1);
		}
		
		HashMap<String, Object> record = selectOne(sql, args);
		if (record == null)
			return null;

		return RecordBeanConverter.convertRecordToBean(record, clazz,
								mapping == null ? resultMapping.get() : mapping);
	}

	/**
	 * 将结构转换成beans
	 */
	public <T> ArrayList<T> selectBeans(Class<T> clazz, String sql, Object ... args)
	{
		if (resultMapping.get() == null)
			// Bug Fix: 当使用单例dao时，不同线程来访问，绑定的ResultMapping不会被正确设置
			// 			因为配置ResultMapping的代码只在构造时被调用并设置进那时的thread local变量里
			//			而对于之后的thread是不起作用的
			//resultMapping.set(new DefaultMapping());
			configResultMapping();
		
		IResultMapping mapping = null;
		if (args.length != 0 && args[args.length-1] instanceof IResultMapping)
		{
			mapping = (IResultMapping) args[args.length-1];
			args = Arrays.copyOf(args, args.length-1);
		}
		
		ArrayList<HashMap<String, Object>> records = selectList(sql, args);
		if (records == null)
			return null;

		return RecordBeanConverter.convertRecordsToBeans(records, clazz, 
										mapping == null ? resultMapping.get() : mapping);
	}

	// =====================================================================================================

	/**
	 * update操作
	 * 返回false代表出错
	 */
	public boolean update(String sql, Object ... args)
	{
		Connection connection = localConnection.get();
		boolean isTxExist = true;
		if (connection == null)
		{
			connection = DbpoolUtil.getConnection();
			if (connection == null)
				return false;
			localConnection.set(connection);
			isTxExist = false;
		}
		PreparedStatement ps = null;

		try
		{
			ps = connection.prepareStatement(sql);
			if (args != null)
				for (int i = 1; i <= args.length; i++)
					ps.setObject(i, args[i - 1]);

			ps.executeUpdate();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		finally
		{
			if (!isTxExist)
			{
				close(connection, ps);
				localConnection.remove();
			}
		}

		return true;
	}

	// =====================================================================================================

	/**
	 * insert操作
	 * 返回-1代表出错，
	 * 返回-2表示没有主键自增值
	 * 否则是AUTO_INCREMENT的值
	 */
	public long insert(String sql, Object ... args)
	{
		Connection connection = localConnection.get();
		boolean isTxExist = true;
		if (connection == null)
		{
			connection = DbpoolUtil.getConnection();
			if (connection == null)
				return -1;
			
			localConnection.set(connection);
			isTxExist = false;
		}
		PreparedStatement ps = null;
		
		long generatedValue = -1;
		try
		{
			ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			if (args != null)
				for (int i = 1; i <= args.length; i++)
					ps.setObject(i, args[i - 1]);

			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			generatedValue = -1;

			// 假定自增主键类型是integer或者long
			if (rs.next())
				generatedValue = rs.getLong(1);
			else 
				//没有主键则返回-2
				generatedValue = -2;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
		finally
		{
			if (!isTxExist)
			{
				close(connection, ps);
				localConnection.remove();
			}
		}

		return generatedValue;
	}

	// =====================================================================================================

	/**
	 * delete操作
	 */
	public boolean delete(String sql, Object ... args)
	{
		// just operate the same as update()
		return update(sql, args);
	}

	// =====================================================================================================
	
	/**
	 * 事务操作方法
	 * 只有所有的事务包括嵌套的
	 * 全部返回true的时候才执行提交
	 * 否则都会回滚
	 * 
	 * 返回false代表出错，事务回滚
	 */
	public boolean tx(int transactionLevel, ITxCallback callback)
	{
		Connection conn = localConnection.get();
		
		if (conn != null)
		{ 
			// 嵌套事务支持
			try
			{
				if (conn.getTransactionIsolation() < transactionLevel)
					conn.setTransactionIsolation(transactionLevel);
				
				if (!callback.run())
					ifAllTxSuccess.set(false);
			}
			catch (SQLException e)
			{
				ifAllTxSuccess.set(false);
				e.printStackTrace();
			}
			
			return ifAllTxSuccess.get();
		}

		Boolean autoCommit = null;
		try
		{
			ifAllTxSuccess.set(true);
			conn = DbpoolUtil.getConnection();
			if (conn == null)
				return false;
			
			localConnection.set(conn);
			//被缓存的数据库连接可能下次还会被分配出去使用
			autoCommit = conn.getAutoCommit();
			conn.setTransactionIsolation(transactionLevel);
			conn.setAutoCommit(false);
			
			boolean result = callback.run();
			if (result && ifAllTxSuccess.get())
			{
				conn.commit();
				return true;
			}
			else
			{
				conn.rollback();
				return false;
			}
		}
		catch(SQLException ex)
		{
			try
			{
				conn.rollback();
			}
			catch (Exception exc)
			{
				exc.printStackTrace();
			}
			return false;
		}
		finally
		{
			try
			{
				if (conn != null)
				{
					if (autoCommit != null)
						conn.setAutoCommit(autoCommit);
					conn.close();
				}
			}
			catch (Throwable t)
			{
				t.printStackTrace();
			}
			finally
			{
				localConnection.remove();
			}
		}
		
	}
	
	
	// =====================================================================================================

	/**
	 * paginate操作
	 * @param:sql就是原来想要查询数据的sql，
	 * 比如：select * from users, infos [, ...][where ... order by id desc ...]
	 * 返回null代表处理出错
	 */
	public Page paginate(long currentPage, long recordPerPage, String sql, Object ... args)
	{
		Page page = new Page();
		page.setCurrentPage(currentPage);
		page.setCountPerPage(recordPerPage);
		Long recordTotalCount = 0L;

		String countSql = sql.replaceFirst("^\\s*\\w*[sS][eE][lL][eE][cC][tT].*[fF][rR][oO][mM]", "select count(*) count from");
		HashMap<String, Object> templist = selectOne(countSql, args);
		
		if (templist == null)
		{
			return null;
		}
		recordTotalCount = (Long) templist.get("count");
		page.setRecordTotalCount(recordTotalCount);
		page.setTotalPages((recordTotalCount + recordPerPage - 1) / recordPerPage);
		page.setResult(selectList(sql + " limit " + (currentPage - 1) * recordPerPage + "," + recordPerPage, args));

		return page;
	}

	/**
	 * paginate by default 10 records per page
	 */
	public Page paginate(long currentPage, String sql, Object ... args)
	{
		return paginate(currentPage, 10, sql, args);
	}

	// =====================================================================================================

	/**
	 * close three things: Connection Statement ResultSet --> executeQuery
	 */
	public void close(Connection conn, Statement st, ResultSet rs)
	{
		try
		{
			if (st != null)
				st.close();

			if (conn != null)
				conn.close();

			if (rs != null)
				rs.close();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * close two things: Connection Statement -->executeUpdate
	 */
	public void close(Connection conn, Statement st)
	{
		try
		{
			if (st != null)
				st.close();

			if (conn != null)
				conn.close();
		}
		catch (SQLException ex)
		{
			ex.printStackTrace();
		}
	}


	protected IResultMapping getResultMapping()
	{
		return resultMapping.get();
	}


	protected synchronized void setResultMapping(IResultMapping resultMapping)
	{
		this.resultMapping.set(resultMapping);
	}
}
