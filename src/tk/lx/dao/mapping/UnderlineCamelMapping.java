/**
 * @FileName UnderlineCamelMapping.java
 * @Package tk.lx.dao.helper
 * @author muxue
 * @date Nov 11, 2015
 */
package tk.lx.dao.mapping;

/**
 * @ClassName: UnderlineCamelMapping
 * @Description 将数据库以下划线分隔的名称转成驼峰式的名称
 * @author muxue
 * @date Nov 11, 2015
 */
public class UnderlineCamelMapping implements IResultMapping
{

	/* 
	 * 这种事固定转换规则的
	 * 将数据库以下划线分隔的名称转成驼峰式的名称
	 * **忽略列名大小写**
	 */
	@Override
	public String getCoName(String columnName)
	{
		columnName = columnName.toLowerCase();
		String[] parts = columnName.split("_");
		StringBuilder builder = new StringBuilder(parts[0]);
		for (int i = 1; i < parts.length; i++)
			builder.append(parts[i].substring(0, 1).toUpperCase()).append(parts[i].substring(1));

		return builder.toString();
	}

}
