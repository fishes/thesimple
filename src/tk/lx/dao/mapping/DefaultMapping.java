/**
 * @FileName DefaultMapping.java
 * @Package tk.lx.dao.helper
 * @author muxue
 * @date Nov 11, 2015
 */
package tk.lx.dao.mapping;

/**
 * @ClassName: DefaultMapping
 * @Description 默认使用的mapping，不对列名进行转换，直接返回
 * @author muxue
 * @date Nov 11, 2015
 */
public class DefaultMapping implements IResultMapping
{

	/* 
	 * 不转换，直接返回
	 */
	@Override
	public String getCoName(String columnName)
	{
		return columnName;
	}

}
