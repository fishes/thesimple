/**
 * @FileName PermissionCheck.java
 * @Package tk.lx.annotation
 * @author muxue
 * @date Nov 25, 2015
 */
package tk.lx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import tk.lx.permission.IRuleChecker;

/**
 * @ClassName: PermissionCheck
 * @Description 权限检查
 * 使用说明:
 * 		1.servlet内是有一个默认的权限检查器的，BaseHttpServlet中的默认实现都是返回true，也就是没有检查
 * 		  子类中可以配置自己的默认权限检查器
 * 		2.rule是使用者自定义的规则字符串，checker负责去解析并执行检查，允许多个checker并存
 * 		  当多个checker并存的时候，根据useDefault标志决定是否需要启用默认的检查器，默认是启用的
 * 		  并且默认检查器是优先于其他检查器执行的
 * 		3.结果：当某一个checker返回false的时候，都将认为是权限不足，相应的action是不会被执行的
 * 		  只有所有指定的检查器都返回true的时候才认为满足权限，之后才会去调用action
 * @author muxue
 * @date Nov 25, 2015
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface PermissionCheck
{
	//自定义的权限规则描述，某些时候并不需要
	String rule() default "";
	//自定义的检查器
	Class<? extends IRuleChecker>[] checker() default {};
	//是否启用全局/默认（整个servlet内）的权限检查器，在多个检查器并存的时候，优先执行
	boolean useDefault() default true;
}
