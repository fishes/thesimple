/**
 * @FileName MultiDao.java
 * @Package tk.lx.annotation
 * @author muxue
 * @date Nov 14, 2015
 */
package tk.lx.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @ClassName: MultiDao
 * @Description 标识相应service为多例
 * @author muxue
 * @date Dec 9, 2015
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MultiService
{

}
