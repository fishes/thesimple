/**
 * @FileName UploadUtil.java
 * @Package tk.lx.util
 * @author muxue
 * @date Dec 2, 2015
 */
package tk.lx.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.http.Part;

/**
 * @ClassName: UploadUtil
 * @Description 上传文件utility
 * @author muxue
 * @date Dec 2, 2015
 */
public class UploadUtil
{
	/**
	 * 得到part文件的扩展名
	 * @param part
	 * @return
	 */
	public static String getFileType(Part part)
	{
		String filename = getFileName(part);
		if (filename == null)
			return null;
		
		int end = filename.lastIndexOf(".");
		if (end == -1)
			return "";

		return filename.substring(end);
	}
	
	/**
	 * 得到part的文件名
	 * @param part
	 * @return
	 */
	public static String getFileName(Part part)
	{
		String header = part.getHeader("Content-Disposition");
		if (header == null || "".equals(header))
			return null;

		String[] fields = header.split(";");
		for (String field : fields)
		{
			String temp = field.trim();
			if (temp.startsWith("filename"))
			{
				int begin = temp.indexOf("\"");
				int end = temp.lastIndexOf("\"");
				return temp.substring(begin+1, end);
			}
		}
		
		return null;
	}
	
	
	/**
	 * 根据时间和和随机数产生一个字符串用作文件名
	 */
	public static String getRandomFileName()
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		String prefix = formatter.format(new Date());
		
		Random random = new Random();
		//1000 - 1999
		int suffix = random.nextInt(1000) + 1000;
		
		return prefix + suffix;
	}
}
