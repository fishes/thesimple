package tk.lx.util.helper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * ClassName: JsonParseHelper
 * Description: 解析JSON文本
 * Date: Nov 1, 2015
 *		
 * 网络实在看得不要不要的
 * 还是修下bug吧，重构，并发时解析会有问题
 * Date: Jan 5, 2016
 * @author muxue
 */

public class JsonParseHelper
{
	/**
	 * 不要使用本类，接口都留在JsonUtil里了
	 * 里面一些public的方法都是为了测试而写的，为了保存测试过程
	 * 这里也就不把权限改掉了
	 */

	// json文本数据
	private String json = "";
	// json文本数据长度
	private int jsonLength = 0;
	// 指示当前读取的位置
	private int index = 0;
	// json数据是否合法
	private boolean isJsonValid = true;

	/*
	 * 应当支持外部获取json的合法性
	 */
	public boolean isJsonInvalid()
	{
		return isJsonValid;
	}

	/*
	 * 为了测试，外部可以访问json域，测试完成后应当关闭这个接口
	 */
	/*public void setJsonText(String jsonText)
	{
		json = jsonText.trim();
		jsonLength = json.length();
	}*/

	/*
	 * 开放给JsonUtil类的接口
	 */
	public Object parseJson(String jsonText)
	{
		cleanup();
		json = jsonText.trim();
		jsonLength = json.length();
		ArrayList<Object> jsonArray = null;
		HashMap<String, Object> jsonObject = null;

		if (jsonLength == 0) 
			return null;
		
		switch (readCharacter())
		{
			case '{':
				jsonObject = parseObject();
				return jsonObject;

			case '[':
				jsonArray = parseArray();
				return jsonArray;
			case '"':
				String string = parseString();
				if(isJsonValid && isEnd())
					return string;
				else 
					return null;
				
			case 't':
				if(parseTrue() && isEnd())
					return true;
				else 
				{
					isJsonValid = false;
					return null;
				}
				
			case 'f':
				if(parseFalse() && isEnd())
					return false;
				else 
				{
					isJsonValid = false;
					return null;
				}
				
			case 'n':
				// 自此可看出即使返回null也不意味着解析出错了
				if(!(parseNull() && isEnd()))
					isJsonValid = false;
				
				return null;
				
			case '0': case '1': case '2': case '3':
			case '4': case '5': case '6': case '7':
			case '8': case '9':
			case '-': case '+': case '.':
				Object number = parseNumber();
				if(isJsonValid && isEnd())
					return number;
				else 
				{
					isJsonValid = false;
					return null;
				}
				
			default:
				isJsonValid = false;
		}

		cleanup();
		return null;
	}

	// =============================================================================

	/**
	 * 解析true，如果解析成功就返回true，否则为false
	 */
	private boolean parseTrue()
	{
		return (readCharacter() == 'r' &&
				readCharacter() == 'u' &&
				readCharacter() == 'e');
	}
	
	/**
	 * 解析false，如果解析成功就返回true，否则为false
	 */
	private boolean parseFalse()
	{
		return (readCharacter() == 'a' &&
				readCharacter() == 'l' &&
				readCharacter() == 's' &&
				readCharacter() == 'e');
	}
	
	/**
	 * 解析null，如果解析成功就返回true，否则为false
	 */
	private boolean parseNull()
	{
		return (readCharacter() == 'u' &&
				readCharacter() == 'l' &&
				readCharacter() == 'l');
	}
	
	/**
	 * 解析number，统一返回double/long类型，在转成拥有某种属性的对象时，再根据相应字段类型转换过去
	 */
	private Number parseNumber()
	{
		StringBuilder builder = new StringBuilder();
		double doubleNumber = -1.0;
		long longNumber = 0L;
		boolean isDouble = false;
		
		//回退一个被读取的数字
		index--;
		char tempChar = readCharacter();
		while (tempChar != ',' && tempChar != ']' && tempChar != '}' && !isEnd())
		{
			if (tempChar == '.')
				// 即使多个小数点也无所谓，底下会触发异常
				isDouble = true;
			builder.append(tempChar);
			tempChar = readCharacter();
		}
		
		if (Character.isDigit(tempChar)) 
			builder.append(tempChar);
		else 
			index--;	
		
		try
		{
			if (isDouble)
			{
				doubleNumber = Double.valueOf(builder.toString());
				return doubleNumber;
			}
			else
			{
				longNumber = Long.valueOf(builder.toString());
				return longNumber;
			}
		}
		catch (NumberFormatException e)
		{
			//not a valid number
			isJsonValid = false;
			return 0L;
		}

	}
	
	/**
	 * 解析string到String， 暂时为了测试方法设置为public，完成后应当设为private
	 */
	private String parseString()
	{
		StringBuilder builder = new StringBuilder();
		char tempChar = '\0';
		
		while (true)
		{
			tempChar = readCharacter();
			if (isEnd() || tempChar == '"')
				break;

			if (tempChar == '\\')
			{
				builder.append(tempChar);
				switch (peekCharacter())
				{
					case '"':
					case '\\':
					case '/':
					case 'b':
					case 'f':
					case 'n':
					case 'r':
					case 't':
						builder.append(readCharacter());
						break;
					case 'u':
						builder.append(readCharacter());
						for (int i = 0; i < 4; i++)
						{
							char temp = readCharacter();
							if (temp >= '0' && temp <= '9' || temp >= 'a' && temp <= 'f' || temp >= 'A' && temp <= 'F')
								builder.append(temp);
							else
							{
								isJsonValid = false;
								return null;
							}
						}
						break;
					default:
						isJsonValid = false;
						return null;
				}
			}
			builder.append(tempChar);
		}

		//字符串以'"'结尾
		if (tempChar == '\"') 
			return builder.toString();	
		else 
		{
			isJsonValid = false;
			return null;
		}
	}

	/*
	 * 解析object到HashMap， 暂时为了测试方法设置为public，完成后应当设为private
	 */
	private HashMap<String, Object> parseObject()
	{
		skipSpaces();
		if (peekCharacter() != '"')
		{
			isJsonValid = false;
			return null;
		}

		HashMap<String, Object> currentObject = new HashMap<>();
		String keyStore = null;
		Object valueStore = null;

		while (peekCharacter() != '}')
		{
			switch (readCharacter())
			{
				case '"':
					keyStore = parseString();
					break;
				case ':':
					skipSpaces();
					switch (readCharacter())
					{
						case 't':
							if (!parseTrue())
							{
								isJsonValid = false;
								return null;
							}
							valueStore = true;
							break;
						case 'f':
							if (!parseFalse())
							{
								isJsonValid = false;
								return null;
							}
							valueStore = false;
							break;
						case 'n':
							if (!parseNull())
							{
								isJsonValid = false;
								return null;
							}
							valueStore = null;
							break;
						case '\"':
							valueStore = parseString();
							if (valueStore == null)
							{
								return null;
							}
							break;
						case '0': case '1': case '2': case '3':
						case '4': case '5': case '6': case '7':
						case '8': case '9':
						case '-': case '+': case '.':
							valueStore = parseNumber();
							if (!isJsonValid)
								return null;
							break;
						case '[':
							valueStore = parseArray();
							break;
						case '{':
							valueStore = parseObject();
							break;
						default:
							isJsonValid = false;
							return null;
					}
					break;
				case ',':
					currentObject.put(keyStore, valueStore);
					keyStore = null;
					valueStore = null;

					skipSpaces();
					if (peekCharacter() != '"')
					{
						isJsonValid = false;
						return null;
					}
					break;
				default:
					isJsonValid = false;
					return null;
			}
			skipSpaces();
		}
		// get rid of '}'
		readCharacter();
		currentObject.put(keyStore, valueStore);

		return currentObject;
	}

	/*
	 * 解析array到ArrayList， 暂时为了测试方法设置为public，完成后应当设为private
	 */
	private ArrayList<Object> parseArray()
	{
		skipSpaces();

		ArrayList<Object> currentArray = new ArrayList<>();
		Object object = null;

		while (peekCharacter() != ']')
		{
			switch (readCharacter())
			{
				case '"':
					object = parseString();
					break;
				case ',':
					currentArray.add(object);
					break;
				case '[':
					object = parseArray();
					break;
				case '{':
					object = parseObject();
					break;
				default:
					isJsonValid = false;
					return null;
			}
			skipSpaces();
		}
		// get rid of ']'
		readCharacter();
		currentArray.add(object);

		return currentArray;
	}

	// =============================================================================
	// 读取辅助方法

	/*
	 * 读取一个字符，并递增index
	 */
	private char readCharacter()
	{
		return json.charAt(index++);
	}

	/*
	 * 读取一个字符而不移动index
	 */
	private char peekCharacter()
	{
		return json.charAt(index);
	}

	/**
	 * 确认是否到达文本末尾
	 */
	private boolean isEnd() {
		return index == jsonLength;
	}
	
	/*
	 * 跳过空格，遇到非空格则停下
	 */
	private void skipSpaces()
	{
		char tempCh = peekCharacter();
		while (Character.isWhitespace(tempCh))
		{
			index++;
			tempCh = peekCharacter();
		}
		return;
	}

	// =============================================================================

	/*
	 * 清理一下static数据
	 */
	private void cleanup()
	{
		json = "";
		jsonLength = 0;
		index = 0;
		isJsonValid = true;
	}

}
