/**
 * @FileName DbPool.java
 * @Package tk.lx.util.helper
 * @author muxue
 * @date Dec 3, 2015
 */
package tk.lx.util.helper;

import java.io.PrintWriter;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.LinkedList;
import java.util.logging.Logger;

import javax.sql.DataSource;

import tk.lx.util.PropertyUtil;

/**
 * @ClassName: DbPool
 * @Description 数据库连接池，重构原先的DbpoolUtility
 * 用法： 
 *	 	1.获取连接 			tk.lx.dao.helper.Connection conn = DbPool.getConnection();
 * 		2.连接使用同往常 		conn.createStatement(...) conn.executeQuery(...) ....... 
 * 		3.连接关闭同往常		conn.close() 
 * 
 * 说明：和之前最大区别就是采用代理换掉了原来的wrapper，从实现上来说，jdk本身的接口代理的局限性还是蛮大的
 * 		拿掉了定时回收的线程，觉得暂时没有自动回收的必要，而且回收策略也是有待考量的
 * @author muxue
 * @date Dec 3, 2015
 */
public class DbPool implements DataSource
{
	
	// =======================================================================
	// 配置项

	// 默认创建5个连接
	private int connMount = 5;
	private String dbUrl = null;
	private String username = null;
	private String password = null;
	private String dbDriver = null;

	// 连接池，增长策略为初始容量的半倍增长
	// 只覆盖了add(E e)和pollFirst()两个方法
	// 如果使用了LinkedList其他的方法可能就会出错
	// <impantant>在扩展的时候要注意到这点</impotant>
	private static LinkedList<Connection> connections = new LinkedList<Connection>()
	{
		private static final long serialVersionUID = 1L;

		@Override
		public boolean add(Connection connection)
		{
			InvocationHandler handler = Proxy.getInvocationHandler(connection);
			if (handler == null)
				return false;
			
			//进入连接池的connection都将置为未被使用的
			//如果出现外部保持一个已被释放的连接的引用，而且恰巧该连接被再次分配
			//那么再次使用就可能会出现问题，然而这应该就不是我的责任了
			((ConnectionProxy) handler).unuse();
			
			return super.add(connection);
		}
		
		@Override
		public Connection pollFirst()
		{
			Connection connection = super.pollFirst();
			if (connection == null)
				return null;
				
			InvocationHandler handler = Proxy.getInvocationHandler(connection);
			if (handler == null)
				return null;
			((ConnectionProxy) handler).use();
			
			return connection;
		}
	};

	/**
	 * 为了在数据库连接代理中能够回收连接
	 */
	public static void recycleConnection(Connection connection)
	{
		connections.add(connection);
	}
	
	public DbPool()
	{
		// 初始化
		try
		{
			connMount = Integer.parseInt(PropertyUtil.use("db.properties").get("DEFAULT_CONNECTIONS"));
			dbUrl = PropertyUtil.use("db.properties").get("DB_URL");
			username = PropertyUtil.use("db.properties").get("USERNAME");
			password = PropertyUtil.use("db.properties").get("PASSWORD");
			dbDriver = PropertyUtil.use("db.properties").get("DBDRIVER");
			Class.forName(dbDriver);
		}
		catch (NumberFormatException e)
		{
			System.err.println("Invalid DEFAULT_CONNECTIONS, change it into 10 ...");
			connMount = 10;
		}
		catch (ClassNotFoundException ex)
		{
			System.err.println("Cannot load the driver ...");
			System.exit(0);
		}
		finally
		{
			if (dbUrl == null || username == null || password == null || dbDriver == null)
			{
				System.err.println("DBURL or USERNAME or PASSWORD or DBDRIVER can not be null ...");
				System.exit(0);
			}
		}

		addConnections(true);

//		// 启动回收线程
//		Thread thread = new Thread(new RunRecycleDbConnections(this));
//		thread.start();
	}

	private void addConnections(boolean isFirst)
	{
		Connection connection = null;
		int increment = isFirst ? connMount /* 默认保持连接数 */: connMount / 2;
		for (int i = 0; i < increment; i++)
		{
			try
			{
				connection = DriverManager.getConnection(dbUrl, username, password);
			}
			catch (SQLException ex)
			{
				ex.printStackTrace();
				return;
			}
			connections.add((Connection)Proxy.newProxyInstance(Connection.class.getClass().getClassLoader(), 
					new Class<?>[] {Connection.class}, new ConnectionProxy(connection)));
		}
	}

//	/**
//	 * 一次性回收多余的连接，控制连接数在现在正在使用的数量（>10）或者10
//	 */
//	public void recycleConnections()
//	{
//		// 新连接池
//		Vector<ConnectionProxy> newpool = new Vector<ConnectionProxy>();
//
//		// 回收连接数至现在活动的连接数
//		if (poolused > connMount)
//		{
//			for (ConnectionProxy conn : connectionProxies)
//			{
//				if (!conn.isUsed())
//				{
//					try
//					{
//						conn.getConnection().close();
//					}
//					catch (SQLException ex)
//					{
//						ex.printStackTrace();
//					}
//					poolsize--;
//				}
//				else
//				{
//					// 把需要的连接放置到newpool中
//					newpool.add(conn);
//				}
//			}
//			connectionProxies = newpool;
//		}
//
//		// 回收连接数至10
//		if (poolused < connMount && poolsize > connMount)
//		{
//			for (ConnectionProxy conn : connectionProxies)
//			{
//				if (!conn.isUsed() && poolsize > 10)
//				{
//					try
//					{
//						conn.getConnection().close();
//					}
//					catch (SQLException ex)
//					{
//						ex.printStackTrace();
//					}
//					poolsize--;
//				}
//				else
//				{
//					// 把需要的连接放置到newpool中
//					newpool.add(conn);
//				}
//			}
//			connectionProxies = newpool;
//		}
//
//	}

	/*
	 * 获取数据库连接
	 */
	@Override
	public synchronized Connection getConnection() throws SQLException
	{
		if (connections.size() > 0)
			return connections.pollFirst();

		// 连接池中连接数不够，需要增长
		addConnections(false);
		return getConnection();
	}

	// ==================================================================================
	// 要求实现的方法，暂时不准备实现
	// ==================================================================================

	/*
	 * 指定账号密码获取一个连接，暂时也不准备启用
	 */
	@Override
	public Connection getConnection(String username, String password) throws SQLException
	{
		return null;
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException
	{
		return null;
	}

	@Override
	public int getLoginTimeout() throws SQLException
	{
		return 0;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException
	{
		return null;
	}

	@Override
	public void setLogWriter(PrintWriter arg0) throws SQLException
	{
	}

	@Override
	public void setLoginTimeout(int arg0) throws SQLException
	{
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException
	{
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException
	{
		return null;
	}

}

///**
// * 数据库连接回收
// * 
// * @ClassName: RunRecycleDbConnections
// * @Description
// * @author muxue
// * @date Dec 3, 2015
// */
//class RunRecycleDbConnections implements Runnable
//{
//	private DbPool dbpool = null;
//	
//	public RunRecycleDbConnections(DbPool pool)
//	{
//		this.dbpool = pool;
//	}
//	
//	@Override
//	public void run()
//	{
//		if (dbpool == null)
//			return;
//		
//		while (true)
//		{
//			try
//			{
//				Thread.sleep(1000 * 60 * 3);
//				dbpool.recycleConnections();
//			}
//			catch (Exception ex)
//			{
//			}
//		}
//	}
//
//}
