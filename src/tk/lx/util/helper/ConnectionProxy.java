package tk.lx.util.helper;

/**
 * ClassName: ConnectionProxy
 * Description:	做些connection的转接，以便连接池的操作
 * Date: Oct 31, 2015
 *
 * 重构Connection，改为由代理实现拦截
 * Date: Dec 3, 2015
 * @author muxue
 */

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.SQLWarning;

public class ConnectionProxy implements InvocationHandler
{

	private Connection connection = null;
	private boolean isUsed = false;

	// =============================================
	// constructor
	public ConnectionProxy(Connection conn)
	{
		this.connection = conn;
	}
	
	public void use()
	{
		this.isUsed = true;
	}
	
	public void unuse()
	{
		this.isUsed = false;
	}
	
	/* 
	 * 数据库连接拦截，处理close方法和其他方法，如果已被关闭，就不允许再次使用
	 */
	@Override
	public Object invoke(Object paramObject, 
			Method paramMethod, Object[] paramArrayOfObject) throws Throwable
	{
		String methodName = paramMethod.getName();
		
		if ("close".equals(methodName))
		{
			DbPool.recycleConnection((Connection)paramObject);
			return null;
		}
		else if ("isClosed".equals(methodName))
		{
			return isUsed;
		}
		else 
		{
			if (!isUsed)
				throw new SQLWarning("The connection is closed ...");
			return paramMethod.invoke(connection, paramArrayOfObject);
		}
	}
	
	
	// =============================================
	// set/get method
	public java.sql.Connection getConnection()
	{
		return connection;
	}

	public boolean isUsed()
	{
		return isUsed;
	}

}
