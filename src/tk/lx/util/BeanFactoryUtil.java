/**
 * @FileName IocContainer.java
 * @Package tk.lx.service
 * @author muxue
 * @date Nov 14, 2015
 */
package tk.lx.util;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentHashMap;

import tk.lx.annotation.MultiDao;
import tk.lx.annotation.MultiService;
import tk.lx.annotation.SingleDao;
import tk.lx.annotation.SingleService;
import tk.lx.dao.BaseDao;
import tk.lx.service.BaseService;
import tk.lx.servlet.BaseHttpServlet;

/**
 * @ClassName: IocContainer
 * @Description 简单管理service && dao实例
 * @author muxue
 * @date Nov 14, 2015
 */
public class BeanFactoryUtil
{
	/**
	 * 管理单例dao的实例
	 */
	private static ConcurrentHashMap<Class<? extends BaseDao>, BaseDao> singletonDaos = 
			new ConcurrentHashMap<>();
	/**
	 * 管理单例service的实例
	 */
	private static ConcurrentHashMap<Class<? extends BaseService>, BaseService> singletonServices = 
			new ConcurrentHashMap<>();
	
	/**
	 * 获取dao的单例 
	 */
	public static BaseDao getSingletonDao(Class<? extends BaseDao> clazz)
	{
		BaseDao dao = singletonDaos.get(clazz);
		
		if (dao == null)
			synchronized (BeanFactoryUtil.class)
			{
				if (dao == null)
					try
					{
						dao = clazz.newInstance();
						singletonDaos.put(clazz, dao);
					}
					catch (InstantiationException | IllegalAccessException e)
					{
						e.printStackTrace();
						return null;
					}
			}
		
		return dao;
	}
	
	
	/**
	 * 获取dao的多例 
	 */
	public static BaseDao getMultiDao(Class<? extends BaseDao> clazz)
	{
		if (clazz != null)
			try
			{
				return clazz.newInstance();
			}
			catch (InstantiationException | IllegalAccessException e)
			{
				e.printStackTrace();
			}
		
		return null;
	}
	
	/**
	 * 获取service的单例 
	 */
	public static BaseService getSingletonService(Class<? extends BaseService> clazz)
	{
		BaseService service = singletonServices.get(clazz);
		
		if (service == null)
			synchronized (BeanFactoryUtil.class)
			{
				if (service == null)
					try
					{
						service = clazz.newInstance();
						singletonServices.put(clazz, service);
					}
					catch (InstantiationException | IllegalAccessException e)
					{
						e.printStackTrace();
						return null;
					}
			}
		
		return service;
	}
	
	
	/**
	 * 获取service的多例 
	 */
	public static BaseService getMultiService(Class<? extends BaseService> clazz)
	{
		if (clazz != null)
			try
			{
				return clazz.newInstance();
			}
			catch (InstantiationException | IllegalAccessException e)
			{
				e.printStackTrace();
			}
		
		return null;
	}
	
	
	/**
	 * 注入符合条件的service和dao实例
	 * **特别注意**
	 * 	现在Base*中已有的代码应当是不会在并发时出错的
	 *  那么在使用单例的时候就要注意继承类中共享资源在并发时是否会出错
	 *  如果会，那么可以简单的使用多例，或者自己去控制一下资源访问
	 */
	public static void injectFields(Object clazzToBeInjected)
	{
		if (clazzToBeInjected == null)
			return;
		else if (!(clazzToBeInjected instanceof BaseHttpServlet) && 
				!(clazzToBeInjected instanceof BaseService)) 
			//这种注入只能发生在BaseHttpServlet和BaseService中
			//也就是说能在BaseHttpServlet和BaseService中注入service和dao
			return;

		Field[] fields = clazzToBeInjected.getClass().getDeclaredFields();
		
		for (Field field : fields)
		{
			Class<?> fieldClass = field.getType();
			Class<? extends BaseDao> subDaoClass = null;
			Class<? extends BaseService> subServiceClass = null;
			
			if (fieldClass.getSuperclass() == BaseDao.class)
				subDaoClass = fieldClass.asSubclass(BaseDao.class);
			else if (fieldClass.getSuperclass() == BaseService.class) 
				subServiceClass = fieldClass.asSubclass(BaseService.class);
			else
				//如果不是BaseDao | BaseService 的继承类，就不去处理
				continue;
			
			try
			{
				if (field.getAnnotation(SingleDao.class) != null)
				{
					field.setAccessible(true);
					field.set(clazzToBeInjected, BeanFactoryUtil.getSingletonDao(subDaoClass));
				}
				else if (field.getAnnotation(MultiDao.class) != null)
				{
					field.setAccessible(true);
					field.set(clazzToBeInjected, BeanFactoryUtil.getMultiDao(subDaoClass));
				}
				else if (field.getAnnotation(SingleService.class) != null)
				{
					field.setAccessible(true);
					field.set(clazzToBeInjected, BeanFactoryUtil.getSingletonService(subServiceClass));
				}
				else if (field.getAnnotation(MultiService.class) != null)
				{
					field.setAccessible(true);
					field.set(clazzToBeInjected, BeanFactoryUtil.getMultiService(subServiceClass));
				}
			}
			catch (SecurityException | IllegalArgumentException | IllegalAccessException e)
			{
				e.printStackTrace();
			}
		}
	}
}
