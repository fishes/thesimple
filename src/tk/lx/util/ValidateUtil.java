package tk.lx.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ClassName: ValidateUtil
 * Description: 提供一些常见的数据校验
 * Date: Oct 31, 2015
 *
 * @author muxue
 */

public class ValidateUtil
{
	/*
	 * 总结一下：
	 * 包括：byte, short, int, long, BigInteger
	 * float, double, BigDecimal
	 * date、string & regexp的有效性（在类似于大小前后长度的概念上）
	 * -->正则中特别有URL和EMAIL的检验
	 * 类似byte的验证时，参数还是保持了int主要是为了调用时传参的方便
	 */

	/**
	 * 验证是否是一个预计大小之内的byte
	 */
	public static boolean validateByte(String value, int min, int max)
	{
		try
		{
			byte temp = Byte.parseByte(value);
			if (temp < min || temp > max)
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是个byte
	 */
	public static boolean validateByte(String value)
	{
		try
		{
			Byte.parseByte(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个预计大小之内的short
	 */
	public static boolean validateShort(String value, int min, int max)
	{
		try
		{
			short temp = Short.parseShort(value);
			if (temp < min || temp > max)
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是个short
	 */
	public static boolean validateShort(String value)
	{
		try
		{
			Short.parseShort(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个预计大小之内的整数
	 */
	public static boolean validateInteger(String value, int min, int max)
	{
		try
		{
			int temp = Integer.parseInt(value);
			if (temp < min || temp > max)
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是个整数
	 */
	public static boolean validateInteger(String value)
	{
		try
		{
			Integer.parseInt(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个预计大小之内的长整数
	 */
	public static boolean validateLong(String value, long min, long max)
	{
		try
		{
			long temp = Long.parseLong(value);
			if (temp < min || temp > max)
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是个长整数
	 */
	public static boolean validateLong(String value)
	{
		try
		{
			Long.parseLong(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个预计大小之内的Biginteger
	 */
	public static boolean validateBigInteger(String value, BigInteger min, BigInteger max)
	{
		try
		{
			BigInteger temp = new BigInteger(value);
			if (temp.compareTo(min) == -1 || temp.compareTo(max) == 1)
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是个Biginteger
	 */
	public static boolean validateBigInteger(String value)
	{
		try
		{
			new BigInteger(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个预计大小之内的float
	 */
	public static boolean validateFloat(String value, double min, double max)
	{
		try
		{
			float temp = Float.parseFloat(value);
			if (temp < min || temp > max)
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是个float
	 */
	public static boolean validateFloat(String value)
	{
		try
		{
			Float.parseFloat(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个预计大小之内的double
	 */
	public static boolean validateDouble(String value, double min, double max)
	{
		try
		{
			double temp = Double.parseDouble(value);
			if (temp < min || temp > max)
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是个double
	 */
	public static boolean validateDouble(String value)
	{
		try
		{
			Double.parseDouble(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个预计大小之内的BigDecimal
	 */
	public static boolean validateBigDecimal(String value, BigDecimal min, BigDecimal max)
	{
		try
		{
			BigDecimal temp = new BigDecimal(value);
			if (temp.compareTo(min) == -1 || temp.compareTo(max) == 1)
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是个BigDecimal
	 */
	public static boolean validateBigDecimal(String value)
	{
		try
		{
			new BigDecimal(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	// ====================================================================================

	private static final String datePattern = "yyyy-MM-dd";

	/**
	 * 验证是否是一个预计前后之内的日期
	 */
	public static boolean validateDate(String value, Date min, Date max)
	{
		try
		{
			Date temp = new SimpleDateFormat(datePattern).parse(value);
			if (temp.before(min) || temp.after(max))
				return false;
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个日期，这里使用yyyy-MM-dd的格式
	 */
	public static boolean validateDate(String value)
	{
		try
		{
			new SimpleDateFormat(datePattern).parse(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个日期，这里使用自定义的格式
	 */
	public static boolean validateDate(String value, String pattern)
	{
		try
		{
			new SimpleDateFormat(pattern).parse(value);
		}
		catch (Exception e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否是一个预计前后之内的日期
	 */
	public static boolean validateDate(String value, String min, String max)
	{

		try
		{
			SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
			validateDate(value, sdf.parse(min), sdf.parse(max));
		}
		catch (ParseException e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证一个值是否是Time类型
	 */
	public static boolean validateTime(String value)
	{
		try
		{
			Time.valueOf(value);
		}
		catch (IllegalArgumentException ex)
		{
			return  false;
		}

		return  true;
	}
	
	/**
	 * 验证一个值是否是TimeStamp类型
	 */
	public static boolean validateTimestamp(String value)
	{
		try
		{
			Timestamp.valueOf(value);
		}
		catch (Exception e)
		{
			return false;
		}
		
		return true;
	}
	

	// ====================================================================================

	/**
	 * 验证两个字符串是否相等
	 */
	public static boolean validateEqualString(String s1, String s2)
	{
		return !(s1 == null || s2 == null || (!s1.equals(s2)));
	}

	/**
	 * 验证是否是指定长度内的非空/空字符串
	 */
	public static boolean validateString(String value, boolean notBlank, int minLen, int maxLen)
	{
		return !(value == null || value.length() < minLen || value.length() > maxLen ||
				notBlank && "".equals(value.trim()));
	}

	/**
	 * 验证是否是指定长度内的非空字符串
	 */
	public static boolean validateString(String value, int minLen, int maxLen)
	{
		return validateString(value, true, minLen, maxLen);
	}

	/**
	 * 验证一个字符串是否为null或者空
	 */
	public static boolean validateString(String value)
	{
		return !(value == null || "".equals(value));
	}

	//
	// //////////////////////////////////////////////////////////////////////////////////////////////////
	//

	// 正则在字符串中还要考虑java本身的转义问题
	private static final String emailAddressPattern = "\\b(^['_A-Za-z0-9-]+(\\.['_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";

	/**
	 * 验证是否符合email格式
	 */
	public static boolean validateEmail(String field)
	{
		return validateRegex(field, emailAddressPattern, false);
	}

	/**
	 * 验证是否符合URL格式
	 */
	public static boolean validateUrl(String value)
	{
		try
		{
			if (value.startsWith("https://"))
				value = "http://" + value.substring(8); // URL doesn't
														// understand the https
														// protocol, hack it
			new URL(value);
		}
		catch (MalformedURLException e)
		{
			return false;
		}

		return true;
	}

	/**
	 * 验证是否符合指定正则表达式
	 */
	// 要设置是否大小写敏感
	public static boolean validateRegex(String value, String regExpression, boolean isCaseSensitive)
	{
		if (value == null)
		{
			return false;
		}
		Pattern pattern = isCaseSensitive ? Pattern.compile(regExpression) : Pattern.compile(regExpression, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(value);

		return matcher.matches();
	}

	/**
	 * 验证是否符合指定正则表达式且大小写敏感
	 */
	public static boolean validateRegex(String value, String regExpression)
	{
		return validateRegex(value, regExpression, true);
	}

}
