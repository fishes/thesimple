/**
 * @FileName DbpoolUtil.java
 * @Package tk.lx.util
 * @author muxue
 * @date Dec 3, 2015
 */
package tk.lx.util;

import java.sql.Connection;
import java.sql.SQLException;

import tk.lx.util.helper.DbPool;

/**
 * @ClassName: DbpoolUtil
 * @Description Dbpool对外暴露一个接口吧，对原先的代码保持接口的统一性
 * @author muxue
 * @date Dec 3, 2015
 */
public class DbpoolUtil
{
	private static DbPool pool = new DbPool();
	
	/**
	 * 获取连接
	 */
	public static Connection getConnection()
	{
		try
		{
			return pool.getConnection();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
}
