package tk.lx.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

/**
 * ClassName: PropertyUtil
 * Description: 配置文件操作类
 * Date: Oct 31, 2015
 *
 * @author muxue
 */

public class PropertyUtil
{
	private static HashMap<String, HashMap<String, String>> propFiles = new HashMap<>();
	private static HashMap<String, String> currentProperties = new HashMap<>();

	private PropertyUtil()
	{
	}

	/**
	 * 加载指定配置文件，并且置入当前使用的properties，直到下一次调用use()
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, String> use(String filename)
	{
		if (!propFiles.containsKey(filename))
		{
			
			Properties properties = new Properties();
			
			try (InputStream inputStream = PropertyUtil.class.getClassLoader().getResourceAsStream(filename))
			{
				if (inputStream == null)
					return new HashMap<>();
				properties.load(inputStream);
			}
			catch (IOException ioE)
			{
				ioE.printStackTrace();
			}

			Enumeration<String> names = (Enumeration<String>) properties.propertyNames();
			HashMap<String, String> props = new HashMap<>();
			while (names.hasMoreElements())
			{
				String key = (String) names.nextElement();
				props.put(key, properties.getProperty(key));
			}

			// 关于下面代码同步的一点说明：
			//      代码分析程序是说这里synchronized不应当以非final实例变量作为获取锁的对象
			//      考虑到若是其他代码块修改了这个实例变量，而且在上一个synchronized还没有退出的时候再次进入的话
			//      那么可能会有些问题
			//
			//      但是这里不存在这样的问题
			synchronized (currentProperties)
			{
				currentProperties = props;
				synchronized (propFiles)
				{
					propFiles.put(filename, props);
				}
			}

			return currentProperties;
		}
		else
		{
			synchronized (currentProperties)
			{
				currentProperties = propFiles.get(filename);
			}
			return currentProperties;
		}
	}

	/**
	 * 获取指定键的值
	 */
	public static String get(String keyname)
	{
		return currentProperties.get(keyname);
	}
}
