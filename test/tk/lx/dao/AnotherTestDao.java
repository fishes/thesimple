/**
 * @FileName TestDao.java
 * @Package tk.lx.dao
 * @author muxue
 * @date Nov 14, 2015
 */
package tk.lx.dao;

import tk.lx.bean.NewUserBean;

/**
 * @ClassName: TestDao
 * @Description 测试基础dao
 * @author muxue
 * @date Nov 14, 2015
 */
public class AnotherTestDao extends BaseDao
{
	public NewUserBean getUser()
	{
		return selectBean(NewUserBean.class, "select * from user where id=?", 2);
	}
}
