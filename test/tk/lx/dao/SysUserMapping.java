/**
 * @FileName SysUserMapping.java
 * @Package tk.lx.dao
 * @author muxue
 * @date Nov 12, 2015
 */
package tk.lx.dao;

import tk.lx.dao.mapping.AbstractCustomMapping;

/**
 * @ClassName: SysUserMapping
 * @Description 映射sys_user表
 * @author muxue
 * @date Nov 12, 2015
 */
public class SysUserMapping extends AbstractCustomMapping
{
	
//	CREATE TABLE `sys_user` (
//	  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
//	  `USER_NAME` varchar(64) DEFAULT NULL COMMENT '用户名',
//	  `USER_PASSWORD` varchar(255) DEFAULT NULL COMMENT '密码',
//	  `WX_USER_NAME` varchar(255) DEFAULT NULL COMMENT '微信账号',
//	  `USER_PHONE` varchar(64) DEFAULT NULL COMMENT '手机号码',
//	  `NAME` varchar(64) DEFAULT NULL COMMENT '用户姓名',
//	  `STATUS` tinyint(1) NOT NULL COMMENT '状态，1-有效，0-过期',
//	  `ROLE` tinyint(1) NOT NULL COMMENT '类型，0-超级管理员，1-店长,2-店员,3-财务,4-加盟店,5-一级代理商,6-二级代理商,7-工厂,9-客户',
//	  `REFEREE` int(11) DEFAULT NULL COMMENT '推荐人 USER_ID',
//	  `FOLLOW` int(11) DEFAULT NULL COMMENT '跟踪人 USER_ID',
//	  `INTERGRATION` int(11) DEFAULT NULL COMMENT '剩余积分',
//	  `REG_DATE` datetime DEFAULT NULL COMMENT '注册时间',
//	  `REMARK` varchar(255) DEFAULT NULL COMMENT '备注',
//	  PRIMARY KEY (`USER_ID`)
//	) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';
	
	/* 
	 * 在此添加sys_user表的映射
	 */
	@Override
	public void makeMappings()
	{
		addMapping("USER_ID", "userId").
		addMapping("USER_NAME", "userName").
		addMapping("USER_PASSWORD", "userPassword").
		addMapping("WX_USER_NAME", "wxUserName").
		addMapping("USER_PHONE", "userPhone").
		addMapping("NAME", "name").
		addMapping("STATUS", "status").
		addMapping("ROLE", "role").
		addMapping("REFEREE", "referee").
		addMapping("FOLLOW", "follow").
		addMapping("INTERGRATION", "intergration").
		addMapping("REG_DATE", "regDate").
		addMapping("REMARK", "remark");
	}
	
}
