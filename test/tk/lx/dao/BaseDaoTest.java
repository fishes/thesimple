/**
 * @FileName BaseDaoTest.java
 * @Package tk.lx.dao
 * @author muxue
 * @date Nov 11, 2015
 */
package tk.lx.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;

import tk.lx.bean.NewUserBean;
import tk.lx.bean.SysUserBean;
import tk.lx.dao.mapping.UnderlineCamelMapping;

/**
 * @ClassName: BaseDaoTest
 * @Description 
 * @author muxue
 * @date Nov 11, 2015
 */
public class BaseDaoTest
{
	private BaseDao dao = new BaseDao()
	{
	};
	
	/**
	 * 
	 */
	@Test
	public void testSelectList1()
	{
		ArrayList<HashMap<String, Object>> result = dao.selectList("select * from difftype");	//in `test` database
		
		//for debug
		System.out.println(result);
	}
	
	/**
	 * 
	 */
	@Test
	public void testSelectList2()
	{
		ArrayList<HashMap<String, Object>> result = dao.selectList("select * from user");
		
		//for debug
		System.out.println(result);
	}

	/**
	 * 
	 */
	@Test
	public void testSelectOne()
	{
		HashMap<String, Object> result = dao.selectOne("select * from user where 1=?", 1);
		
		//for debug
		System.out.println(result);
	}

	/**
	 * 使用默认的映射器
	 */
	@Test
	public void testSelectBean1()
	{
		NewUserBean bean = dao.selectBean(NewUserBean.class, "select * from user where id=?", 1);
		
		//for debug
		System.out.println(bean);
	}
	
	/**
	 * 使用下划线转驼峰式的映射器
	 */
	@Test
	public void testSelectBean2()
	{
		SysUserBean bean = dao.selectBean(SysUserBean.class, "select * from sys_user where user_id=?", 1, new UnderlineCamelMapping());
		
		//for debug
		System.out.println(bean);
	}
	
	/**
	 * 使用自定义的映射器
	 */
	@Test
	public void testSelectBean3()
	{
		SysUserBean bean = dao.selectBean(SysUserBean.class, "select * from sys_user where user_id=?", 1, new SysUserMapping());
		
		//for debug
		System.out.println(bean);
	}
	

	/**
	 * 
	 */
	@Test
	public void testSelectBeans1() throws SQLException
	{
		ArrayList<NewUserBean> result = dao.selectBeans(NewUserBean.class, "select * from user");
		
		//for debug
		System.out.println(result);
	}
	
	
	/**
	 * 
	 */
	@Test
	public void testSelectBeans2() throws SQLException
	{
		ArrayList<SysUserBean> result = dao.selectBeans(SysUserBean.class, "select * from sys_user", new UnderlineCamelMapping());
		
		//for debug
		System.out.println(result);
	}

	/**
	 * 
	 * @throws SQLException 
	 */
	@Test
	public void testUpdate() throws SQLException
	{
		dao.update("update user set name='hello world' where id=?", 1);
	}

	/**
	 * 
	 */
	@Test
	public void testInsert() throws SQLException
	{
		dao.insert("INSERT INTO `user` VALUES ("
				+ " ?, ?, ?, ?,"
				+ " ?, '78.9')", 1000, "name", "pass", "2015-11-12", "2015-11-07 23:31:07");
	}

	/**
	 * 
	 */
	@Test
	public void testDelete() throws SQLException
	{
		//先使用testInsert()插入数据
		dao.delete("delete from user where id=?", 1000);
	}

	/**
	 * 
	 */
	@Test
	public void testPaginate() throws SQLException
	{
		Page page1 = dao.paginate(1, 3, "select * from user where name=? order by id desc", "name");
		Page page2 = dao.paginate(2, 5, "select * from user where name=? order by id desc", "name");
		Page page3 = dao.paginate(1, 100, "select * from user where name=? order by id desc", "name");
		
		//for debug
		System.out.println(page1);
		System.out.println(page2);
		System.out.println(page3);
	}

	/**
	 * 测试事务 ，返回true，应该改动数据库 
	 */
	@Test
	public void testTx1()
	{
		dao.tx(Connection.TRANSACTION_READ_COMMITTED, new ITxCallback()
		{
			
			@Override
			public boolean run()
			{
				dao.update("update user set name=? where id=?", "tx1", "3");
				return true;
			}
		});
	}
	
	/**
	 * 测试事务，返回false，不应该改动数据库 
	 */
	@Test
	public void testTx2()
	{
		dao.tx(Connection.TRANSACTION_READ_COMMITTED, new ITxCallback()
		{
			
			@Override
			public boolean run()
			{
				dao.update("update user set name=? where id=?", "tx2", "3");
				return false;
			}
		});
	}
	
	
	/**
	 * 测试事务，事务嵌套
	 */
	@Test
	public void testTx3()
	{
		dao.tx(Connection.TRANSACTION_READ_COMMITTED, new ITxCallback()
		{
			
			@Override
			public boolean run()
			{
				dao.update("update user set name=? where id=?", "tx-nested-1", "1");
				dao.tx(Connection.TRANSACTION_REPEATABLE_READ, new ITxCallback()
				{
					
					@Override
					public boolean run()
					{
						dao.update("update user set name=? where id=?", "tx-nested-2", "2");
						return true;
					}
				});
				return true;
			}
		});
	}
	
	/**
	 * 测试事务，事务嵌套
	 */
	@Test
	public void testTx4()
	{
		dao.tx(Connection.TRANSACTION_READ_COMMITTED, new ITxCallback()
		{
			
			@Override
			public boolean run()
			{
				dao.update("update user set name=? where id=?", "2222", "1");
				dao.tx(Connection.TRANSACTION_REPEATABLE_READ, new ITxCallback()
				{
					
					@Override
					public boolean run()
					{
						dao.update("update user set name=? where id=?", "3333", "2");
						return false;
					}
				});
				return true;
			}
		});
	}
	
	/**
	 * 测试事务，事务嵌套
	 */
	@Test
	public void testTx5()
	{
		dao.tx(Connection.TRANSACTION_READ_COMMITTED, new ITxCallback()
		{
			
			@Override
			public boolean run()
			{
				dao.update("update user set name=? where id=?", "2222", "1");
				dao.tx(Connection.TRANSACTION_REPEATABLE_READ, new ITxCallback()
				{
					
					@Override
					public boolean run()
					{
						dao.update("update user set name=? where id=?", "3333", "2");
						return true;
					}
				});
				return false;
			}
		});
	}
	
	/**
	 * 测试事务，事务嵌套
	 */
	@Test
	public void testTx6()
	{
		dao.tx(Connection.TRANSACTION_READ_COMMITTED, new ITxCallback()
		{
			
			@Override
			public boolean run()
			{
				dao.update("update user set name=? where id=?", "2222", "1");
				dao.tx(Connection.TRANSACTION_REPEATABLE_READ, new ITxCallback()
				{
					
					@Override
					public boolean run()
					{
						dao.update("update user set name=? where id=?", "3333", "2");
						return true;
					}
				});
				return true;
			}
		});
	}
 }
