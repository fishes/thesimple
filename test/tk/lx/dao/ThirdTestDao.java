/**
 * @FileName ThirdTestDao.java
 * @Package tk.lx.dao
 * @author muxue
 * @date Dec 2, 2015
 */
package tk.lx.dao;

import java.util.List;

import tk.lx.bean.ClassBean;
import tk.lx.dao.mapping.AbstractCustomMapping;
import tk.lx.dao.mapping.IResultMapping;

/**
 * @ClassName: ThirdTestDao
 * @Description 测试ResultMapping的新机制
 * @author muxue
 * @date Dec 2, 2015
 */
public class ThirdTestDao extends BaseDao
{
	@Override
	public void configResultMapping()
	{
		setResultMapping(new AbstractCustomMapping()
		{

			@Override
			public void makeMappings()
			{
				addMapping("ClassID", "classID").addMapping("ClassName", "className").addMapping("Type", "type").addMapping("Belong", "belong").addMapping("TopicNumber", "topicNumber").addMapping("reserved1", "reserved1").addMapping("reserved1", "reserved2");
			}
		});
	}

	public static void main(String[] args)
	{
		test1();
		System.out.println("====================================================================");
		test2();
	}

	private static void test1()
	{
		BaseDao dao = new ThirdTestDao();

		List<ClassBean> beans = dao.selectBeans(ClassBean.class, "select * from classtable");

		for (ClassBean bean : beans)
		{
			System.out.println(bean);
		}
	}
	
	private static void test2()
	{
		BaseDao dao = new ThirdTestDao();

		//No ClassName <--> className mapping ...
		List<ClassBean> beans = dao.selectBeans(ClassBean.class, "select * from classtable", 
				new IResultMapping()
				{
					
					@Override
					public String getCoName(String columnName)
					{
						if(columnName != null && !"".equals(columnName))
						{
							if ("ClassName".equals(columnName))
								return null;
							
							return columnName.substring(0, 1).toLowerCase()
									+ columnName.substring(1);
						}
						return null;
					}
				});

		for (ClassBean bean : beans)
		{
			System.out.println(bean);
		}
	}
}
