/**
 * @FileName TestService.java
 * @Package tk.lx.service
 * @author muxue
 * @date Nov 14, 2015
 */
package tk.lx.service;

import java.util.ArrayList;
import java.util.List;

import tk.lx.annotation.MultiDao;
import tk.lx.annotation.SingleDao;
import tk.lx.bean.NewUserBean;
import tk.lx.dao.AnotherTestDao;
import tk.lx.dao.Page;
import tk.lx.dao.TestDao;

/**
 * @ClassName: TestService
 * @Description 测试基础服务
 * @author muxue
 * @date Nov 14, 2015
 */
public class TestService extends BaseService
{
	@SingleDao
	public TestDao testDao;
	@SingleDao
	public AnotherTestDao anotherTestDao;
	
	@SingleDao
	public TestDao testDao1;
	@SingleDao
	public TestDao testDao2;
	@SingleDao
	public TestDao testDao3;
	
	
	@MultiDao
	public TestDao testMDao1;
	@MultiDao
	public TestDao testMDao2;
	@MultiDao
	public TestDao testMDao3;
	
	public void getUser()
	{
		System.out.println(testDao.getUser());
		System.out.println(anotherTestDao.getUser());
	}
	
	public List<NewUserBean> getUsers()
	{
		ArrayList<NewUserBean> list = new ArrayList<>();
		list.add(testDao.getUser());
		list.add(anotherTestDao.getUser());
		
		return list;
	}
	
	public Page getUserPage(long currentPage, long conutPerPage)
	{
		return testDao.paginate(currentPage, 3, "select * from user");
	}
}
