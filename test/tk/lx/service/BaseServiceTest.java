/**
 * @FileName BaseServiceTest.java
 * @Package tk.lx.service
 * @author muxue
 * @date Nov 14, 2015
 */
package tk.lx.service;

import org.junit.Assert;
import org.junit.Test;

/**
 * @ClassName: BaseServiceTest
 * @Description 
 * @author muxue
 * @date Nov 14, 2015
 */
public class BaseServiceTest
{
	private TestService service = new TestService();
	
	/**
	 * 
	 */
	@Test
	public void testGetUser()
	{
		service.getUser();
	}

	/**
	 * 测试单例是否是单例
	 */
	@Test
	public void testIsSingleton()
	{
		Assert.assertTrue(service.testDao == service.testDao1);
		Assert.assertTrue(service.testDao == service.testDao2);
		Assert.assertTrue(service.testDao == service.testDao3);
	}
	
	
	/**
	 * 测试多例是否是多例
	 */
	@Test
	public void testIsMultiInstance()
	{
		Assert.assertTrue(service.testDao != service.testMDao1);
		Assert.assertTrue(service.testDao != service.testMDao2);
		Assert.assertTrue(service.testDao != service.testMDao3);
		Assert.assertTrue(service.testMDao1 != service.testMDao2);
		Assert.assertTrue(service.testMDao1 != service.testMDao3);
		Assert.assertTrue(service.testMDao2 != service.testMDao3);
	}
}
