/**
 * Project Name: TheSimple
 * File Name: JsonUtilTest.java
 * Package Name: tk.lx.util
 */
package tk.lx.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;

import org.junit.Test;

import tk.lx.bean.UserBean;

/**
 * ClassName: JsonUtilTest
 * Description: 
 * Date: Oct 31, 2015
 *
 * @author muxue
 */
public class JsonUtilTest
{

	@Test
	public void testStringify()
	{
		HashMap<String, Object> hashMap = new HashMap<>();
		//common
		hashMap.put("abc", "aaA");
		hashMap.put("num", +123456);
		
		//collection
		ArrayList<UserBean> users = new ArrayList<>();
		UserBean bean = new UserBean();
		bean.setUserAccount("account");
		bean.setUserEmail(null);
		bean.setUserId(1234);
		bean.setUserIfDepositBook(true);
		bean.setUserName("username");
		bean.setUserNickname(111111111L);
		bean.setUserPassword((short) -1234);
		bean.setUserPhone(2.234f);
		bean.setUserSign(55.66);
		users.add(bean);
		users.add(bean);
		hashMap.put("arraylist", users);
		
		//map
		HashMap<String, Object> userMap = new HashMap<>();
		userMap.put("user1", bean);
		userMap.put("user2", bean);
		hashMap.put("map", userMap);
		
		System.out.println(JsonUtil.stringify(hashMap));
	}

	@Test
	public void testStringify2()
	{
		LinkedHashMap<String, Object> hashMap = new LinkedHashMap<>();
		hashMap.put("errCode", 0);
		hashMap.put("errMsg", "Something Wrong");
		hashMap.put("data", new ArrayList<String>());
		
		System.out.println(JsonUtil.stringify(hashMap));
	}
	
	@Test
	public void testStringify3()
	{
		System.out.println(JsonUtil.stringify(new LinkedHashMap<String, Object>()));
	}
	
	@Test
	public void testStringify4()
	{
		System.out.println(JsonUtil.stringify(new Object()));
	}
	
	@Test
	public void testStringify5()
	{
		System.out.println(JsonUtil.stringify(123.456));
	}
	
	@Test
	public void testStringify6()
	{
		System.out.println(JsonUtil.stringify("Hello world!!!中文"));
	}
	
	@Test
	public void testStringify7()
	{
		System.out.println(JsonUtil.stringify(true));
	}
	
	@Test
	public void testStringify8()
	{
		System.out.println(JsonUtil.stringify(null));
	}
	
	@Test
	public void testParseJsonToBean()
	{
		UserBean bean = JsonUtil.parseJsonToBean(Matcher.quoteReplacement("{\"userId\":1234567,\"userNickname\":-12345,\"userSign\":"+
																			"12.456,\"userIfDepositBook\":true,\"userAccount\":\"account\"," + 
																			"\"userEmail\":\"email\",\"userPassword\":124}"), UserBean.class);
		System.out.println("UserAccount			" + bean.getUserAccount());
		System.out.println("UserEmail			" + bean.getUserEmail());
		System.out.println("UserId				" + bean.getUserId());
		System.out.println("UserIfDepositBook	" + bean.isUserIfDepositBook());
		System.out.println("UserNickname		" + bean.getUserNickname());
		System.out.println("UserPassword		" + bean.getUserPassword());
		System.out.println("UserSign			" + bean.getUserSign());
	}

	
	@Test
	public void testParseJsonToBeans()
	{
		String jsonText = Matcher.quoteReplacement("[{\"userId\":1234567,\"userNickname\":12345,\"userSign\":12.456,\"userIfDepositBook"
													+ "\":true,\"userAccount\":\"account\",\"userEmail\":\"email\",\"userPassword"
													+ "\":124},{\"userId\":1234567,\"userNickname\":12345,\"userSign\":12.456,"
													+ "\"userIfDepositBook\":true,\"userAccount\":\"account\",\"userEmail\":\"email"
													+ "\",\"userPassword\":124},{\"userId\":1234567,\"userNickname\":12345,\"userSign"
													+ "\":12.456,\"userIfDepositBook\":true,\"userAccount\":\"account\",\"userEmail\":"
													+ "\"email\",\"userPassword\":124}]");
		ArrayList<UserBean> beans = JsonUtil.parseJsonToBeans(jsonText, UserBean.class);
		
		for (UserBean iBean : beans)
		{
			UserBean bean = (UserBean)iBean;
			System.out.println("UserAccount			" + bean.getUserAccount());
			System.out.println("UserEmail			" + bean.getUserEmail());
			System.out.println("UserId				" + bean.getUserId());
			System.out.println("UserIfDepositBook	" + bean.isUserIfDepositBook());
			System.out.println("UserNickname		" + bean.getUserNickname());
			System.out.println("UserPassword		" + bean.getUserPassword());
			System.out.println("UserSign			" + bean.getUserSign());
			System.out.println("UserName			" + bean.getUserName());
			System.out.println("UserPhone			" + bean.getUserPhone());
			System.out.println("===============================================");
		}
	}

}
