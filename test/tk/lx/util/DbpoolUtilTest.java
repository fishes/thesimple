/**
 * @FileName DbpoolUtilTest.java
 * @Package tk.lx.util
 * @author muxue
 * @date Dec 3, 2015
 */
package tk.lx.util;

import java.sql.Connection;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;

/**
 * @ClassName: DbpoolUtilTest
 * @Description 
 * @author muxue
 * @date Dec 3, 2015
 */
public class DbpoolUtilTest
{

	@Test
	public void test()
	{
		Connection connection = DbpoolUtil.getConnection();
		Assert.assertNotNull(connection);
	}

	
	@Test
	public void testConnNumBy10()
	{
		for (int count = 0; count < 10; count++)
		{
			System.out.println(DbpoolUtil.getConnection());
		}
	}
	
	
	@Test
	public void testConnNumBy20() throws SQLException
	{
		//效果：应该发生一次连接增长，增加5个
		for (int count = 0; count < 15; count++)
		{
			System.out.println(DbpoolUtil.getConnection());
		}
		
		//效果：再次发生连接增长，增加5个，在10次循环中每个重复使用一次
		for (int count = 0; count < 10; count++)
		{
			Connection connection = DbpoolUtil.getConnection();
			System.out.println(connection);
			connection.close();
		}
	}
}
