/**
 * Project Name: TheSimple
 * File Name: JsonParseHelperTest.java
 * Package Name: tk.lx.util.helper
 */
package tk.lx.util.helper;

import java.util.regex.Matcher;

import org.junit.Test;

/**
 * ClassName: JsonParseHelperTest
 * Description: 
 * Date: Nov 1, 2015
 *
 * @author muxue
 */
public class JsonParseHelperTest
{
	
	/**
	 * 简单类型的测试
	 */
	@Test
	public void testParseJson1()
	{
		String jsonText1 = Matcher.quoteReplacement("true");
		Object result1 = new JsonParseHelper().parseJson(jsonText1);
		System.out.println(result1);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result1 != null ? result1.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText2 = Matcher.quoteReplacement("truetr324");
		Object result2 = new JsonParseHelper().parseJson(jsonText2);
		System.out.println(result2);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result2 != null ? result2.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText3 = Matcher.quoteReplacement("false");
		Object result3 = new JsonParseHelper().parseJson(jsonText3);
		System.out.println(result3);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result3 != null ? result3.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText4 = Matcher.quoteReplacement("false21");
		Object result4 = new JsonParseHelper().parseJson(jsonText4);
		System.out.println(result4);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result4 != null ? result4.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText5 = Matcher.quoteReplacement("null");
		Object result5 = new JsonParseHelper().parseJson(jsonText5);
		System.out.println(result5);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result5 != null ? result5.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText6 = Matcher.quoteReplacement("null x");
		Object result6 = new JsonParseHelper().parseJson(jsonText6);
		System.out.println(result6);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result6 != null ? result6.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText7 = Matcher.quoteReplacement("+.23");
		Object result7 = new JsonParseHelper().parseJson(jsonText7);
		System.out.println(result7);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result7 != null ? result7.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText8 = Matcher.quoteReplacement("-15");
		Object result8 = new JsonParseHelper().parseJson(jsonText8);
		System.out.println(result8);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result8 != null ? result8.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText9 = Matcher.quoteReplacement("+0.3");
		Object result9 = new JsonParseHelper().parseJson(jsonText9);
		System.out.println(result9);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result9 != null ? result9.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText10 = Matcher.quoteReplacement("\"+0.3\"");
		Object result10 = new JsonParseHelper().parseJson(jsonText10);
		System.out.println(result10);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result10 != null ? result10.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		String jsonText11 = Matcher.quoteReplacement("\"hello world+.23\"");
		Object result11 = new JsonParseHelper().parseJson(jsonText11);
		System.out.println(result11);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result11 != null ? result11.getClass().getName() : "null"));
		System.out.println("=======================================================================");
		
		//unclosed quote
		String jsonText12 = Matcher.quoteReplacement("\"hello world+.23");
		Object result12 = new JsonParseHelper().parseJson(jsonText12);
		System.out.println(result12);
		System.out.println("Is json text is invalid " + new JsonParseHelper().isJsonInvalid());
		System.out.println("And the type of result is " + (result12 != null ? result12.getClass().getName() : "null"));
		System.out.println("=======================================================================");
	}
	
	
	@Test
	public void tetsParseJson2()
	{
		String jsonText = "{\"Consume\": {\"Moneys\": {\"totalMoney\": \"0.01\" },  "
				+ "\"record\": [ {\"AppID\": \"11\", \"CreateTime\": \"2008-09-05T08:49:18.063+08:00\", "
				+ "\"GiveEgg\": \"0\", \"ID\": \"714833524\", \"OrderState\": \"2\", \"OrderStateName\": "
				+ "\"失败\", \"OrderType\": \"3\", \"OrderTypeName\": \"实物交易订单\", \"PayID\": \"3\", \"PayWayID\": "
				+ "\"1\", \"PayWayName\": \"快钱在线支付\", \"TotalMoney\": \"0.01\", \"TotalNumber\": \"0\", \"TradeSource\": "
				+ "\"商城购物\", \"UserID\": "
				+ "\"668288112\" }, {\"AppID\": \"11\", \"CreateTime\": \"2008-09-05T08:46:12.813+08:00\", "
				+ "\"GiveEgg\": \"0\", \"ID\": \"1350320533\", \"OrderState\": \"3\", \"OrderStateName\": \"处理中\", "
				+ "\"OrderType\": \"3\", \"OrderTypeName\": \"实物交易订单\", \"PayID\": \"7\", \"PayWayID\": \"2\", "
				+ "\"PayWayName\": \"XXX邮政储蓄所\", \"TotalMoney\": \"0.01\", \"TotalNumber\": \"0\", \"TradeSource\": "
				+ "\"商城购物\", \"UserID\": \"668288112\" }, {\"AppID\": \"18\", \"CreateTime\": \"2008-09-05T08:40:55.703+08:00\", "
				+ "\"GiveEgg\": \"0\", \"ID\": \"1413965649\", \"OrderState\": \"1\", \"OrderStateName\": \"成功\", \"OrderType\": "
				+ "\"4\", \"OrderTypeName\": \"同步交易订单\", \"PayID\": \"2\", \"PayWayID\": \"1\", \"PayWayName\": \"财付通在线支付\","
				+ " \"TotalMoney\": \"0.01\", \"TotalNumber\": \"0\", \"TradeSource\": \"同步课程\", \"UserID\": \"668288112\" },"
				+ " {\"AppID\": \"11\", \"CreateTime\": \"2008-09-05T08:39:29.127+08:00\", \"GiveEgg\": \"0\", \"ID\": \"83430389\","
				+ " \"OrderState\": \"2\", \"OrderStateName\": \"失败\", \"OrderType\": \"3\", \"OrderTypeName\": \"实物交易订单\", "
				+ "\"PayID\": \"3\", \"PayWayID\": \"1\", \"PayWayName\": \"快钱在线支付\", \"TotalMoney\": \"0.01\", \"TotalNumber\":"
				+ " \"0\", \"TradeSource\": \"商城购物\", \"UserID\": \"668288112\" }, {\"AppID\": \"11\", \"CreateTime\": "
				+ "\"2008-09-05T08:38:33.28+08:00\", \"GiveEgg\": \"0\", \"ID\": \"1206699325\", \"OrderState\": \"2\", "
				+ "\"OrderStateName\": \"失败\", \"OrderType\": \"3\", \"OrderTypeName\": \"实物交易订单\", \"PayID\": \"3\", "
				+ "\"PayWayID\": \"1\", \"PayWayName\": \"快钱在线支付\", \"TotalMoney\": \"0.01\", \"TotalNumber\": \"0\", "
				+ "\"TradeSource\": \"商城购物\", \"UserID\": \"668288112\" }, {\"AppID\": \"11\", \"CreateTime\": "
				+ "\"2008-09-05T08:31:54.453+08:00\", \"GiveEgg\": \"0\", \"ID\": \"795858378\", \"OrderState\": \"2\","
				+ " \"OrderStateName\": \"失败\", \"OrderType\": \"3\", \"OrderTypeName\": \"实物交易订单\", \"PayID\": \"3\", "
				+ "\"PayWayID\": \"1\", \"PayWayName\": \"快钱在线支付\", \"TotalMoney\": \"0.01\", \"TotalNumber\": \"0\", "
				+ "\"TradeSource\": \"商城购物\", \"UserID\": \"668288112\" } ], \"results\": {\"totalRecords\": \"6\" } }}";
		
		Object object = new JsonParseHelper().parseJson(jsonText);
		System.out.println(object);
	}
	
	@Test
	public void testParseJson3()
	{
		String jsonText =  "{\"userId\":1234567,\"userNickname\":-12345,\"userSign\":"+
				"12.456,\"userIfDepositBook\":true,\"userAccount\":\"account\"," + 
				"\"userEmail\":\"email\",\"userPassword\":124}";
		Object object = new JsonParseHelper().parseJson(jsonText);
		System.out.println(object);
	}
	
	//===========================================================================================================
	//new test ↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑
	//===========================================================================================================
	
	
//	/*
//	 * 测试object的解析是否正常
//	 */
//	@Test
//	public void testParseObject()
//	{
//		new JsonParseHelper().cleanup();
//		String jsonText = Matcher.quoteReplacement("\"hello\":\"world\",\"object\":{\"ni\":\"hao\"}}");
//		new JsonParseHelper().setJsonText(jsonText);
//		HashMap<String, Object> hashMap = new JsonParseHelper().parseObject();
//		System.out.println(hashMap);
//		Assert.assertNotNull(hashMap);
//	}
//
//	
//	/*
//	 * 测试array的解析是否正常 
//	 */
//	@Test
//	public void testParseArray()
//	{
//		new JsonParseHelper().cleanup();
//		String jsonText = Matcher.quoteReplacement("\"hello\",[\"world\",[\"object\",{\"ni\":\"hao\"}]]]");
//		new JsonParseHelper().setJsonText(jsonText);
//		ArrayList<Object> arrayList = new JsonParseHelper().parseArray();
//		System.out.println(arrayList);
//		Assert.assertNotNull(arrayList);
//	}
//	
//	/*
//	 * 测试string的解析是否正常
//	 * 
//	 * 这个测试可能会有异常的，由于字符串边界问题。需要在调试的过程看结果
//	 */
//	@Test
//	public void testParseString()
//	{
//		new JsonParseHelper().cleanup();
//		//我靠，两边都要转义，我的"怎么写进去...特别注意下java的字符串也做好转义的考虑
//		String jsonText = Matcher.quoteReplacement("abc\u456a\"");
//		new JsonParseHelper().setJsonText(jsonText);
//		System.out.println(new JsonParseHelper().parseString());
//	}
//
//	/**
//	 * 实际json数据的测试
//	 * 
//	 */
//	
//	/*
//	 * 一个bean的json数据
//	 */
//	@Test
//	public void testBeanJson()
//	{
//		new JsonParseHelper().cleanup();
//		String jsonText = Matcher.quoteReplacement("{\"userId\":+.123456,\"userNickname\":\"nickname\",\"userSign\":-12.34,\"userIfDepositBook\":true,\"" + 
//													"userAccount\":null,\"userEmail\":false,\"userPassword\":\"password\"}");
//		Object object = new JsonParseHelper().parseJson(jsonText);
//		System.out.println(object);
//		Assert.assertNotNull(object);
//	}
//	
//	/*
//	 * 一个bean集合的json数据
//	 */
//	@Test
//	public void testBeansJson()
//	{
//		new JsonParseHelper().cleanup();
//		String jsonText = Matcher.quoteReplacement("[{\"userId\":123456,\"userNickname\":\"nickname\",\"userSign\":12.34,\"userIfDepositBook\":true,\"" + 
//													"userAccount\":null,\"userEmail\":false,\"userPassword\":\"password\"}," + 
//													"{\"userId\":123456,\"userNickname\":\"nickname\",\"userSign\":12.34,\"userIfDepositBook\":true,\"" + 
//													"userAccount\":null,\"userEmail\":false,\"userPassword\":\"password\"}," +
//													"{\"userId\":123456,\"userNickname\":\"nickname\",\"userSign\":12.34,\"userIfDepositBook\":true,\"" + 
//													"userAccount\":null,\"userEmail\":false,\"userPassword\":\"password\"}]");
//		Object object = new JsonParseHelper().parseJson(jsonText);
//		Assert.assertNotNull(object);
//	}
//	
//	/*
//	 * 一个bean集合的json数据
//	 */
//	@Test
//	public void testBeansJson2()
//	{
//		new JsonParseHelper().cleanup();
//		String jsonText = Matcher.quoteReplacement("{\"num\":123456,\"abc\":\"aaA\",\"map\":{\"user2\":{\"userAccount\":\"account\",\"userEmail\":null,\"userId\":1234,"
//													+ "\"userIfDepositBook\":true,\"userName\":\"username\",\"userNickname\":111111111,"
//													+ "\"userPassword\":1234,\"userPhone\":2.234,\"userSign\":55.66},\"user1\":{\"userAccount\":"
//													+ "\"account\",\"userEmail\":null,\"userId\":1234,\"userIfDepositBook\":true,\"userName\":"
//													+ "\"username\",\"userNickname\":111111111,\"userPassword\":1234,\"userPhone\":2.234,"
//													+ "\"userSign\":55.66}},\"arraylist\":[{\"userAccount\":\"account\",\"userEmail\":null,"
//													+ "\"userId\":1234,\"userIfDepositBook\":true,\"userName\":\"username\",\"userNickname\":111111111,"
//													+ "\"userPassword\":1234,\"userPhone\":2.234,\"userSign\":55.66},{\"userAccount\":\"account\",\"userEmail"
//													+ "\":null,\"userId\":1234,\"userIfDepositBook\":true,\"userName\":\"username\",\"userNickname\":111111111,"
//													+ "\"userPassword\":1234,\"userPhone\":2.234,\"userSign\":55.66}]}");
//		Object object = new JsonParseHelper().parseJson(jsonText);
//		Assert.assertNotNull(object);
//	}
}
