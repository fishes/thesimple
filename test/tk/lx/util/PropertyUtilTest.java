/**
 * Project Name: TheSimple
 * File Name: PropertyUtilTest.java
 * Package Name: tk.lx.util
 */
package tk.lx.util;

import org.junit.Test;

/**
 * ClassName: PropertyUtilTest
 * Description: 
 * Date: Oct 31, 2015
 *
 * @author muxue
 */
public class PropertyUtilTest
{

	/**
	 * Test method for {@link tk.lx.util.PropertyUtil#get(java.lang.String)}.
	 */
	@Test
	public void testGetAndUse()
	{
		//web工程中，代码文件夹的东西被发布到/WEB-INF/classes下	--> 看工程属性
		System.out.println(PropertyUtil.use("db.properties").get("DB_URL"));
		System.out.println(PropertyUtil.get("USERNAME"));
		System.out.println(PropertyUtil.get("PASSWORD"));
		System.out.println(PropertyUtil.use("test.properties").get("test"));
		System.out.println(PropertyUtil.get("USERNAME"));
		System.out.println(PropertyUtil.use("db.properties").get("USERNAME"));
		System.out.println(PropertyUtil.get("USERNAME"));
	}

}
