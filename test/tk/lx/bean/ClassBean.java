/**
 * @FileName ClassBean.java
 * @Package jsp.finalwork.lx.bean
 * @author muxue
 * @date Dec 1, 2015
 */
package tk.lx.bean;


/**
 * @ClassName: ClassBean
 * @Description 分类bean
 * @author muxue
 * @date Dec 1, 2015
 */
public class ClassBean
{
//	CREATE TABLE `classtable` (
//	  `ClassID` int(11) NOT NULL AUTO_INCREMENT COMMENT '类ID',
//	  `ClassName` varchar(255) NOT NULL COMMENT '类名字',
//	  `Type` int(11) NOT NULL COMMENT '类别（1为大类，0为小类）',
//	  `Belong` int(11) NOT NULL COMMENT '归属（只有当Type为0时）',
//	  `TopicNumber` int DEFAULT 0 COMMENT '主题数量',
//	  `reserved1` varchar(255) DEFAULT NULL,
//	  `reserved2` varchar(255) DEFAULT NULL,
//	  PRIMARY KEY (`ClassID`)
//	) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
	
	private int classID;
	private String className;
	private int type;
	private int belong;
	private int topicNumber;
	private String reserved1;
	private String reserved2;
	
	
	public int getClassID()
	{
		return classID;
	}
	public void setClassID(int classID)
	{
		this.classID = classID;
	}
	public String getClassName()
	{
		return className;
	}
	public void setClassName(String className)
	{
		this.className = className;
	}
	public int getType()
	{
		return type;
	}
	public void setType(int type)
	{
		this.type = type;
	}
	public int getBelong()
	{
		return belong;
	}
	public void setBelong(int belong)
	{
		this.belong = belong;
	}
	public int getTopicNumber()
	{
		return topicNumber;
	}
	public void setTopicNumber(int topicNumber)
	{
		this.topicNumber = topicNumber;
	}
	public String getReserved1()
	{
		return reserved1;
	}
	public void setReserved1(String reserved1)
	{
		this.reserved1 = reserved1;
	}
	public String getReserved2()
	{
		return reserved2;
	}
	public void setReserved2(String reserved2)
	{
		this.reserved2 = reserved2;
	}
	@Override
	public String toString()
	{
		return "ClassBean [classID=" + classID + ", className=" + className + ", type=" + type + ", belong=" + belong + ", topicNumber=" + topicNumber + ", reserved1=" + reserved1 + ", reserved2=" + reserved2 + "]";
	}
}
