/**
 * @FileName SysUserBean.java
 * @Package tk.lx.bean
 * @author muxue
 * @date Nov 11, 2015
 */
package tk.lx.bean;

import java.sql.Timestamp;

/**
 * @ClassName: SysUserBean
 * @Description 
 * @author muxue
 * @date Nov 11, 2015
 */
public class SysUserBean
{
//	CREATE TABLE `sys_user` (
//	  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
//	  `USER_NAME` varchar(64) DEFAULT NULL COMMENT '用户名',
//	  `USER_PASSWORD` varchar(255) DEFAULT NULL COMMENT '密码',
//	  `WX_USER_NAME` varchar(255) DEFAULT NULL COMMENT '微信账号',
//	  `USER_PHONE` varchar(64) DEFAULT NULL COMMENT '手机号码',
//	  `NAME` varchar(64) DEFAULT NULL COMMENT '用户姓名',
//	  `STATUS` tinyint(1) NOT NULL COMMENT '状态，1-有效，0-过期',
//	  `ROLE` tinyint(1) NOT NULL COMMENT '类型，0-超级管理员，1-店长,2-店员,3-财务,4-加盟店,5-一级代理商,6-二级代理商,7-工厂,9-客户',
//	  `REFEREE` int(11) DEFAULT NULL COMMENT '推荐人 USER_ID',
//	  `FOLLOW` int(11) DEFAULT NULL COMMENT '跟踪人 USER_ID',
//	  `INTERGRATION` int(11) DEFAULT NULL COMMENT '剩余积分',
//	  `REG_DATE` datetime DEFAULT NULL COMMENT '注册时间',
//	  `REMARK` varchar(255) DEFAULT NULL COMMENT '备注',
//	  PRIMARY KEY (`USER_ID`)
//	) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';
	
	private int userId;
	private String userName;
	private String userPassword;
	private String wxUserName;
	private String userPhone;
	private String name;
	private boolean status;
	private boolean role;
	private int referee;
	private int follow;
	private int intergration;
	private Timestamp regDate;
	private String remark;
	
	public int getUserId()
	{
		return userId;
	}
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public String getUserPassword()
	{
		return userPassword;
	}
	public void setUserPassword(String userPassword)
	{
		this.userPassword = userPassword;
	}
	public String getWxUserName()
	{
		return wxUserName;
	}
	public void setWxUserName(String wxUserName)
	{
		this.wxUserName = wxUserName;
	}
	public String getUserPhone()
	{
		return userPhone;
	}
	public void setUserPhone(String userPhone)
	{
		this.userPhone = userPhone;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public boolean getStatus()
	{
		return status;
	}
	public void setStatus(boolean status)
	{
		this.status = status;
	}
	public boolean getRole()
	{
		return role;
	}
	public void setRole(boolean role)
	{
		this.role = role;
	}
	public int getReferee()
	{
		return referee;
	}
	public void setReferee(int referee)
	{
		this.referee = referee;
	}
	public int getFollow()
	{
		return follow;
	}
	public void setFollow(int follow)
	{
		this.follow = follow;
	}
	public int getIntergration()
	{
		return intergration;
	}
	public void setIntergration(int intergration)
	{
		this.intergration = intergration;
	}
	public Timestamp getRegDate()
	{
		return regDate;
	}
	public void setRegDate(Timestamp regDate)
	{
		this.regDate = regDate;
	}
	public String getRemark()
	{
		return remark;
	}
	public void setRemark(String remark)
	{
		this.remark = remark;
	}
	@Override
	public String toString()
	{
		return "SysUserBean [userId=" + userId + ", userName=" + userName + ", userPassword=" + userPassword + ", wxUserName=" + wxUserName + ", userPhone=" + userPhone + ", name=" + name + ", status=" + status + ", role=" + role + ", referee=" + referee + ", follow=" + follow + ", intergration=" + intergration + ", regDate=" + regDate + ", remark=" + remark + "]";
	}
}
