/**
 * @FileName TypeBean.java
 * @Package tk.lx.bean
 * @author muxue
 * @date Nov 11, 2015
 */
package tk.lx.bean;

import java.math.BigInteger;

/**
 * @ClassName: TypeBean
 * @Description
 * @author muxue
 * @date Nov 11, 2015
 */
public class TypeBean
{
	private int id;
	private String name;
	private int age1;
	private int age2;
	private int age3;
	private int age4;
	private int age5;
	private BigInteger age6;
	private float num1;
	private double num2;
	private double num3;

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public int getAge1()
	{
		return age1;
	}

	public void setAge1(int age1)
	{
		this.age1 = age1;
	}

	public int getAge2()
	{
		return age2;
	}

	public void setAge2(int age2)
	{
		this.age2 = age2;
	}

	public int getAge3()
	{
		return age3;
	}

	public void setAge3(int age3)
	{
		this.age3 = age3;
	}

	public int getAge4()
	{
		return age4;
	}

	public void setAge4(int age4)
	{
		this.age4 = age4;
	}

	public int getAge5()
	{
		return age5;
	}

	public void setAge5(int age5)
	{
		this.age5 = age5;
	}

	public BigInteger getAge6()
	{
		return age6;
	}

	public void setAge6(BigInteger age6)
	{
		this.age6 = age6;
	}

	public float getNum1()
	{
		return num1;
	}

	public void setNum1(float num1)
	{
		this.num1 = num1;
	}

	public double getNum2()
	{
		return num2;
	}

	public void setNum2(double num2)
	{
		this.num2 = num2;
	}

	public double getNum3()
	{
		return num3;
	}

	public void setNum3(double num3)
	{
		this.num3 = num3;
	}

	@Override
	public String toString()
	{
		return "TypeBean [id=" + id + ", name=" + name + ", age1=" + age1 + ", age2=" + age2 + ", age3=" + age3 + ", age4=" + age4 + ", age5=" + age5 + ", age6=" + age6 + ", num1=" + num1 + ", num2=" + num2 + ", num3=" + num3 + "]";
	}
}
