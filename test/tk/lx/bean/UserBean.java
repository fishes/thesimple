/**
 * Project Name: TheSimple
 * File Name: UserBean.java
 * Package Name: tk.lx.bean
 */
package tk.lx.bean;

/**
 * ClassName: UserBean
 * Description: 
 * Date: Oct 31, 2015
 *
 * @author muxue
 */
public class UserBean
{
	private String userAccount;
	private String userEmail;
	private int userId;
	private boolean userIfDepositBook;
	private String userName;
	private long userNickname;
	private short userPassword;
	private float userPhone;
	private double userSign;
	
	public String getUserAccount()
	{
		return userAccount;
	}
	public void setUserAccount(String userAccount)
	{
		this.userAccount = userAccount;
	}
	public String getUserEmail()
	{
		return userEmail;
	}
	public void setUserEmail(String userEmail)
	{
		this.userEmail = userEmail;
	}
	public int getUserId()
	{
		return userId;
	}
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public boolean isUserIfDepositBook()
	{
		return userIfDepositBook;
	}
	public void setUserIfDepositBook(boolean userIfDepositBook)
	{
		this.userIfDepositBook = userIfDepositBook;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public long getUserNickname()
	{
		return userNickname;
	}
	public void setUserNickname(long userNickname)
	{
		this.userNickname = userNickname;
	}
	public short getUserPassword()
	{
		return userPassword;
	}
	public void setUserPassword(short userPassword)
	{
		this.userPassword = userPassword;
	}
	public float getUserPhone()
	{
		return userPhone;
	}
	public void setUserPhone(float userPhone)
	{
		this.userPhone = userPhone;
	}
	public double getUserSign()
	{
		return userSign;
	}
	public void setUserSign(double userSign)
	{
		this.userSign = userSign;
	}
	@Override
	public String toString()
	{
		return "UserBean [userAccount=" + userAccount + ", userEmail=" + userEmail + ", userId=" + userId + ", userIfDepositBook=" + userIfDepositBook + ", userName=" + userName + ", userNickname=" + userNickname + ", userPassword=" + userPassword + ", userPhone=" + userPhone + ", userSign=" + userSign + "]";
	}
}
