/**
 * @FileName NewUserBean.java
 * @Package tk.lx.bean
 * @author muxue
 * @date Nov 11, 2015
 */
package tk.lx.bean;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * @ClassName: NewUserBean
 * @Description 
 * @author muxue
 * @date Nov 11, 2015
 */
public class NewUserBean
{
	private int id;
	private String name;
	private String pass;
	private Date date;
	private Timestamp time;
	private double length;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getPass()
	{
		return pass;
	}
	public void setPass(String pass)
	{
		this.pass = pass;
	}
	public Date getDate()
	{
		return date;
	}
	public void setDate(Date date)
	{
		this.date = date;
	}
	public Timestamp getTime()
	{
		return time;
	}
	public void setTime(Timestamp time)
	{
		this.time = time;
	}
	public double getLength()
	{
		return length;
	}
	public void setLength(double length)
	{
		this.length = length;
	}
	
	
	@Override
	public String toString()
	{
		return "NewUserBean [id=" + id + ", name=" + name + ", pass=" + pass + ", date=" + date + ", time=" + time + ", length=" + length + "]";
	}
	
	
}
