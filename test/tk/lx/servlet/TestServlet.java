/**
 * Project Name: TheSimple
 * File Name: TestServlet.java
 * Package Name: tk.lx.servlet
 */
package tk.lx.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.PermissionCheck;
import tk.lx.annotation.RequestParam;
import tk.lx.annotation.SingleService;
import tk.lx.permission.IRuleChecker;
import tk.lx.service.TestService;
import tk.lx.servlet.helper.UploadFileInfo;


/**
 * ClassName: TestServlet
 * Description: 用于测试BaseHttpServlet
 * Date: Oct 31, 2015
 *
 * @author muxue
 */
@WebServlet(urlPatterns="/test/*")
@MultipartConfig
public class TestServlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	
	@SingleService
	TestService service;
	
	//===================================================================================
	
	@Override
	protected void config()
	{
		setPermissionChecker(new IRuleChecker()
		{
			
			@Override
			public boolean checkRule(String rule, BaseHttpServlet servlet)
			{
				System.out.println("In default permission checker ...");
				return true;
			}
		});
	}
	
	
	//===================================================================================
	
	@ActionMapping("noindex")
	public void index()
	{
		System.out.println("in index method ...");
		renderToErrorPage("成功调用index方法");
	}
	
	@ActionMapping("hello")
	//public void anotherMethod(@RequestParam("hello")String hello, int noAnno)
	public void anotherMethod(@RequestParam("hello")String hello,  String noAnno)
	{
		System.out.println("in anotherMethod method ...");
		System.out.println(noAnno);
		renderToErrorPage("成功调用anotherMethod方法，参数hello是" + hello, "error.jsp");
	}
	
	
	//===================================================================================
	
	@ActionMapping("user")
	public void getUser()
	{
		setAttr("users", service.getUsers());
		renderPage("/WEB-INF/test/user.jsp");
	}
	
	//===================================================================================
	
//	@ActionMapping("page")
//	public void getUserPage(@Param("currentPage") String currentPage)
//	{
//		if (ValidateUtil.validateLong(currentPage))
//		{
//			long cp = Long.valueOf(currentPage.equals("") ? "1" : currentPage);
//			Page page = service.getUserPage(cp, 3);
//			
//			setPaginateInfo("test/page", page);
//			setAttr("users", page.getResult());
//			renderPage("/WEB-INF/test/pages.jsp");
//		}
//		else
//		{
//			renderToErrorPage("错误的参数值：" + currentPage);
//		}
//	}
	
	
	//===================================================================================
	
	@ActionMapping("rule1")
	@PermissionCheck(rule="hahaha")
	public void checkPermission1()
	{
		renderToErrorPage("Nothing to show you ... here in checkPermission1()");
	}
	
	@ActionMapping("rule2")
	@PermissionCheck(rule="hahaha", checker = {MyRuleChecker.class, MySecondRuleChecker.class})
	public void checkPermission2()
	{
		renderToErrorPage("Nothing to show you ... here in checkPermission2()");
	}
	
	@ActionMapping("rule3")
	@PermissionCheck(rule="continue", checker = {MyRuleChecker.class, MySecondRuleChecker.class})
	public void checkPermission3()
	{
		renderToErrorPage("Nothing to show you ... here in checkPermission3()");
	}
	
	@ActionMapping("rule4")
	@PermissionCheck(rule="continue", checker = {})
	public void checkPermission4()
	{
		renderToErrorPage("Nothing to show you ... here in checkPermission4()");
	}
	
	@ActionMapping("rule5")
	@PermissionCheck(rule="continue", checker = {}, useDefault=false)
	public void checkPermission5()
	{
		renderToErrorPage("Nothing to show you ... here in checkPermission5()");
	}
	
	//===================================================================================
	
	@ActionMapping("jsonError")
	public void backWithJsonError() throws IOException
	{
		renderJSONError("This is return by renderJSONError() ...");
	}
	
	
	@ActionMapping("jsonData")
	public void backWithJSONData() throws IOException
	{
		renderJSONResult(0, service.getUsers());
	}
	
	//===================================================================================
	
	@ActionMapping("upload")
	public void getFile() throws IOException, ServletException
	{
		System.out.println(saveFile("file"));
		renderJSONError("Ok, uploaded ...");
	}
	
	@ActionMapping("uploadByName")
	public void getFileSaveByName() throws IOException, ServletException
	{
		System.out.println(saveFile("fileByName", "myfilename"));
		renderJSONError("Ok, uploaded ...");
	}
	
	@ActionMapping("uploads")
	public void getFiles() throws IOException, ServletException
	{
		System.out.println(saveFiles());
		renderJSONError("Ok, uploaded ...");
	}
	
	//===================================================================================
	
	@ActionMapping("jsp")
	public void fowardJsp()
	{
		renderPage("/WEB-INF/test/show.jsp");
	}
	
	//===================================================================================

	@ActionMapping("umupload")
	public void getFileFromUMEditor() throws IOException, ServletException
	{
		UploadFileInfo fileInfo = saveFile("upfile");
		if (fileInfo == null)
			renderUMError();
		else 
			renderUMSuccess(fileInfo);
	}
	
}

//===================================================================================

class MyRuleChecker implements IRuleChecker
{

	@Override
	public boolean checkRule(String rule, BaseHttpServlet servlet)
	{
		System.out.println(rule);
		if ("continue".equals(rule))
			return true;
		else
			return false;
	}
	
}

class MySecondRuleChecker implements IRuleChecker
{

	@Override
	public boolean checkRule(String rule, BaseHttpServlet servlet)
	{
		System.out.println(rule);
		if ("continue".equals(rule))
			return true;
		else
			return false;
	}
	
}
