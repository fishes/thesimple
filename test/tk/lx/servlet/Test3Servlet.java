/**
 * @FileName Test3Servlet.java
 * @Package tk.lx.servlet
 * @author muxue
 * @date Jan 4, 2016
 */
package tk.lx.servlet;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;

/**
 * @ClassName: Test3Servlet
 * @Description  专门测试getParam()之前设置在threadlocal中，
 * 					与线程池配合使用会不会造成内存泄漏
 * @author muxue
 * @date Jan 4, 2016
 */
@WebServlet(urlPatterns="/test/param/*")
public class Test3Servlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	
	public void index() throws IOException
	{
		System.out.println("Current thread id is : " + Thread.currentThread().getId() + 
							"  and the value is " + getParam("param"));
		renderJSONError(getParam("param"));
	}
}
