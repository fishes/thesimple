/**
 * @FileName Test2Servlet.java
 * @Package tk.lx.servlet
 * @author muxue
 * @date Dec 12, 2015
 */
package tk.lx.servlet;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import javax.servlet.annotation.WebServlet;

import tk.lx.annotation.ActionMapping;
import tk.lx.annotation.RequestParam;

/**
 * @ClassName: Test2Servlet
 * @Description 
 * @author muxue
 * @date Dec 12, 2015
 */
@WebServlet(urlPatterns="/test2/*")
public class Test2Servlet extends BaseHttpServlet
{
	private static final long serialVersionUID = 1L;
	
	
	@ActionMapping("integer")
	public void method(@RequestParam("a") byte a,
						@RequestParam("b") short b,
						@RequestParam("c") int c,
						@RequestParam("d") long d,
						@RequestParam("e") BigInteger e)
	{
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(e);
	}
	
	@ActionMapping("decimal")
	public void method1(@RequestParam("a") float a,
						@RequestParam("b") double b, 
						@RequestParam("c") BigDecimal c)
	{
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
	}
	
	@ActionMapping("datetime")
	public void method2(@RequestParam("a") Date a, 
						@RequestParam("b") Time b, 
						@RequestParam("c") Timestamp c)
	{
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
	}
	
}
