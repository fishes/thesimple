<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>用户列表</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
</head>

<body ng-app="UsersModule">
<div class="container">
	<table border="1">
		<tr>
			<td>id</td>
			<td>name</td>
			<td>date</td>
			<td>time</td>
			<td>length</td>
		</tr>
		<c:forEach var="user" items="${users}">
			<tr>
				<td>${user.id}</td>
				<td>${user.name}</td>
				<td>${user.date}</td>
				<td>${user.time}</td>
				<td>${user.length}</td>
			</tr>
		</c:forEach>
	</table>
</div>
<%@ include file="../jsp/paginate.jsp" %>
<script type="text/javascript" src="framework/jquery/jquery-1.11.3.min.js" />
<script type="text/javascript" src="framework/angular/angular.min.js" />
<script type="text/javascript" src="js/admin/UsersCtrl.js" />
</body>
</html>
